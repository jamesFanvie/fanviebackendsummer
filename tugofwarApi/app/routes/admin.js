var express = require('express');
var router = express.Router();
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var adminController = require('../controllers/adminController');
const authentication = require('../../middlewares/jwtTokenAdmin');
router.post('/signUp', adminController.signUp);
router.post('/login', adminController.login);
router.post('/addGame', multipartMiddleware)
router.post('/addGame', authentication, adminController.addGame);
router.post('/addTriviaGame', multipartMiddleware)
router.post('/addTriviaGame', authentication, adminController.addTriviaGame);
router.post('/editGame', authentication, multipartMiddleware, adminController.editGame);
router.post('/startGame', adminController.startGame);
router.post('/startTriviaGame', adminController.startTriviaGame);
router.get('/gameList', adminController.gameList);
router.get('/triviaGameList', adminController.triviaGameList);
router.post('/rematch', multipartMiddleware);
router.post('/rematch', authentication, adminController.rematch);
router.get('/finishedGame', authentication, adminController.finishedGame);
router.get('/userDetails/:id/:teamName', authentication, adminController.userDetails);
router.post('/addTeam',authentication, adminController.addTeam)
router.get('/getTeams', adminController.getTeams)
router.get('/getMainTeams', adminController.getMainTeams)
router.get('/deleteAddGame/:id', authentication, adminController.deleteAddGame);
router.post('/editTeams', authentication, adminController.editTeams)
router.post('/sendNotification', authentication, adminController.pushNotification);
router.get('/userInRematch', adminController.userInRematch);
router.get('/applicationImages', adminController.noGameIdExists);
router.get('/allNotificationsUsers',authentication,  adminController.allNotificationsUsers);
module.exports = router;
