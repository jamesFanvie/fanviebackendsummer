const constants = require("../config/constants.js");
module.exports = () => {
  switch (process.env.NODE_ENV) {
    case 'dev':
      return {
        SITEURL: constants.STAGINGURL,
        MONGODB: constants.MONGODB.STAGING.URL,
        FCM: constants.FCM,
        FROM_MAIL: constants.FROM_MAIL.LOCALHOST,
        SMTP_CRED: constants.SMTP_CRED.LOCALHOST,
        JWTOKEN: constants.JWTOKENDEV,
        STAGEURL: constants.STAGEURL,
        EXPIRY: constants.key.tokenExpiry,
        firebase : constants.firebase,
        LOADFORMPATH:constants.loadFromPath,
        accountSid : constants.accountSidLocal,
        authToken : constants.authTokenLocal,
        twilioFrom : constants.twilioFromLocal,
        country : constants.countryLocal,
      };
    case 'production':
      return {
        SITEURL: constants.LIVEURL,
        MONGODB: constants.MONGODB.LIVE.URL,
        FCM: constants.FCM,
        FROM_MAIL: constants.FROM_MAIL.LIVE,
        SMTP_CRED: constants.SMTP_CRED.LIVE,
        JWTOKEN: constants.JWTOKENLIVE,
        STAGEURL: constants.PRODURL,
        EXPIRY: constants.key.tokenExpiry,
        LOADFORMPATH:constants.loadFromPath,
        accountSid : constants.accountSid,
        authToken : constants.authToken,
        twilioFrom : constants.twilioFrom,
        country : constants.countryProd,
      };
    case 'staging':
      return {
        SITEURL: constants.STAGINGURL,
        MONGODB: constants.MONGODB.STAGING.URL,
        FCM: constants.FCM.SERVERKEY,
        FROM_MAIL: constants.FROM_MAIL.STAGING,
        SMTP_CRED: constants.SMTP_CRED.STAGING,
        JWTOKEN: constants.JWTOKENSTAGING,
        STAGEURL: constants.STAGEURL,
        EXPIRY: constants.key.tokenExpiry,

        LOADFORMPATH:constants.loadFromPath,
        accountSid : constants.accountSid,
        authToken : constants.authToken,
        twilioFrom : constants.twilioFrom,
        country : constants.countryProd,

      };
    case 'test':
      return {
        SITEURL: constants.LIVEURL,
        MONGODB: constants.MONGODB.TEST.URL,
        FCM: constants.FCM,
        FROM_MAIL: constants.FROM_MAIL.LOCALHOST,
        SMTP_CRED: constants.SMTP_CRED.LOCALHOST,
        JWTOKEN: constants.JWTOKENLOCAL,
        STAGEURL: constants.STAGEURL,
        EXPIRY: constants.key.tokenExpiry,

        LOADFORMPATH:constants.loadFromPath,
        accountSid : constants.accountSid,
        authToken : constants.authToken,
        twilioFrom : constants.twilioFrom,
        country : constants.countryProd,

      };
    default:
      return {
        SITEURL: constants.LOCALURL,
        MONGODB: constants.MONGODB.LOCALHOST.URL,
        FCM: constants.FCM,
        FROM_MAIL: constants.FROM_MAIL.LOCALHOST,
        SMTP_CRED: constants.SMTP_CRED.LOCALHOST,
        JWTOKEN: constants.JWTOKENLOCAL,
        STAGEURL: constants.STAGEURL,
        EXPIRY: constants.key.tokenExpiry,

        LOADFORMPATH:constants.loadFromPath,
        accountSid : constants.accountSidLocal,
        authToken : constants.authTokenLocal,
        twilioFrom : constants.twilioFromLocal,
        country : constants.countryLocal,
      };
  }
};
