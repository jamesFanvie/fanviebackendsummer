const Jwt = require('jsonwebtoken');
const env = require('../config/env')();
const privateKey = env.JWTOKEN;
module.exports = (req, res, next) => {
  var token;
  if (req.headers && req.headers.token) {
    const credentials = req.headers.token;
    token = credentials;
  }else {
    return res.status(403).json({
      resStatus: 0,
      resMessage: 'ACCESS DENIED !! You are not authorize to access this Resource',
    });
  }

  Jwt.verify(token,privateKey, (err, token) => {
    if (err) {
      return res.status(401).json({
        resStatus: 0,
        resMessage: 'The token is not valid',
      });
    }else {
      req.token = token;
      next();
    }
  });
};
