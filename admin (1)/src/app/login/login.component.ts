import { Component, OnInit, NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationServiceService } from '../authentication-service.service'

//import { AlertService, AuthenticationService } from '../_services';

@Component({ templateUrl: 'login.component.html', selector: 'login-root', styleUrls: ['login.component.css'] })
export class LoginComponent implements OnInit {
    userForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    email: string;
    password: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationServiceService
    ) { }
    // private alertService: AlertService) {}

    ngOnInit() {
        //this.email="samee";
        this.userForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
        //reset login status
        this.authenticationService.logout();
        //get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    // get f() { return this.userForm.controls; }

    onSubmit() {

        debugger;
        console.log("login tugofwar admin part=============")
        this.loading = true;
        this.authenticationService.login(this.email, this.password)
            .pipe(first())
            .subscribe(
                data => {
                   
                    //this.router.navigate(['/departments',department.id])
                    console.log("data=tugofwar admin========", data)
                    if (data.status)
                        this.router.navigate(['dashboard/addgame']);
                    else {

                       alert(data.message)
                         this.router.navigate(['dashboard/addgame']);
                    }
                   
                },
                error => {
                    console.log('error', error)
                });
    }
}
