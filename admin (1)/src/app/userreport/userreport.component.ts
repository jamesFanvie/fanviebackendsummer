import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service'
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-user-report',
  templateUrl: './userreport.component.html',
  styleUrls: []
})
export class UserreportComponent implements OnInit {

  userReports = [];
  Counter = 5;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.commonService.getUsersReport()
      .pipe(first())
      .subscribe(
        data => {
          this.userReports = data;
          console.log("data============", this.userReports)
          //this.router.navigate(['/departments',department.id])
          //this.router.navigate(['/dashboard','users']);
        },
        error => {
          console.log('error', error)
        });
  }

}
