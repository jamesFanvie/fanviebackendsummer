const express = require('express');
const bodyParser = require('body-parser');
const socketio = require('socket.io');
var users = require('./app/routes/users');
var admin = require('./app/routes/admin');
var path = require('path');
var logger = require('morgan');
var cors = require('cors');
var cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const compression = require('compression');
const gameModel = require('./app/models/games');
const triviaGameModel = require('./app/models/triviaGames');
const ObjectId = require('objectid');
mongoose.Promise = global.Promise;
global.APP_PATH = path.resolve(__dirname);

// create express app
const app = express();
app.use(compression());
// parse requests of content-type - application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: true }))
// parse requests of content-type - application/json
// app.use(bodyParser.json())
//let server = http.Server(app);
// Configuring the database
const env = require('./config/env')();
mongoose.Promise = global.Promise;
// Connecting to the database
//env.MONGODB
//mongoose.connect('mongodb://localhost:27017/tugofwar');

mongoose.connect('mongodb://localhost:27017/tugofwar', {
  useNewUrlParser: true
}).then(() => {
  console.log("Successfully connected to the database");
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// define a simple route
app.get('/', (req, res) => {
  res.json({ "message": "Welcome to Tug of war application." });
});
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
  next();
});

// Require Notes routes
app.use('/api/users', users);
app.use('/api/admin', admin);
// require('./app/routes/routes.js')(app);
app.options('/*', cors())
// listen for requests
const port = 8092;
const server = app.listen(port, () => console.log('App listening on this port ' + port));

const websocket = socketio(server); //Initiate Socket
websocket.set('transports', ['websocket', 'polling']);
websocket.on('connection', (socket) => {
  console.log('Greetings from RN app--------', socket.id);
  socket.on('abc', function (data) {
    console.log("*&*&*&*&*&*&* inside socket on abc function _+_+_++__+ ", data);
    console.log(data.gametype);
    if(data.gametype === "tugOfWar" ) {
          let countdown = data.duration + 9;
    //  websocket.emit('gameStarted',{started:true,gameId:data._id})
    //  websocket.emit('gameStarted2',{started:true,gameId:data._id})
    console.log("start game data===tugOfWar", data._id);

    let timerVal = setInterval(function () {
      countdown--;
      websocket.emit('gameStarted', { started: true, gameId: data._id, data: data })
      //  websocket.emit('timer', { countdown: data.duration,timerStarted:true });
      console.log(countdown - 7, 'timer running ***countdown** ', countdown);
      if (countdown == data.duration) {
        let timerVal1 = setInterval(function () {
          data.duration--;
          console.log("=countdown==========", countdown);
          websocket.emit('gameStarted2', { started: true, gameId: data._id })
          websocket.emit('timer', { countdown: data.duration, timerStarted: true });
          console.log('timer running 45***** ', data.duration);
          if (data.duration == 0) {
            websocket.emit('gameStarted2', { started: false, gameId: data._id })
            console.log('countdown running3454 ***** ', data.duration, "data._id ", data._id);
            clearInterval(timerVal1)

            ////////////////////////////////////////////////////////////////////
            let timerVal2 = setInterval(function () {
              gameModel.find({ _id: data._id }, (err, result) => {
                console.log("result[0].startGame", result[0].startGame, "reslut", result);

                if (result && result.length) {
                  if (result[0].startGame == '2') {
                    gameModel.updateOne({ _id: data._id }, { $set: { startGame: 1 } }, { new: true },
                      (err, obj) => {
                        if (obj) {
                          console.log("obj", obj);
                          console.log("startGame change to 1 IN case when no user play game");
                          clearInterval(timerVal2)
                        }
                      })
                  } else {
                    console.log("startGame is already 1 & Finished");
                    clearInterval(timerVal2)
                  }
                }
              })
            }, 3000);
            ////////////////////////////////////////////////////////////////////

          }
        }, 1000);
        //websocket.emit('stopTimer',{started:false})
        console.log('countdown running ***** ', data.duration);
        clearInterval(timerVal)
      }
    }, 1000);
    }

     if(data.gametype === "triviaGame" ) {
          let countdown = data.duration + 9;
    //  websocket.emit('gameStarted',{started:true,gameId:data._id})
    //  websocket.emit('gameStarted2',{started:true,gameId:data._id})
    console.log("start game data===", data._id);

    let timerVal = setInterval(function () {
      countdown--;
      websocket.emit('gameStarted', { started: true, gameId: data._id, data: data })
      //  websocket.emit('timer', { countdown: data.duration,timerStarted:true });
      console.log(countdown - 7, 'timer running ***countdown** ', countdown);
      if (countdown == data.duration) {
        let timerVal1 = setInterval(function () {
          data.duration--;
          console.log("=countdown==========", countdown);
          websocket.emit('gameStarted2', { started: true, gameId: data._id })
          websocket.emit('timer', { countdown: data.duration, timerStarted: true });
          console.log('timer running 45***** ', data.duration);
          if (data.duration == 0) {
            websocket.emit('gameStarted2', { started: false, gameId: data._id })
            console.log('countdown running3454 ***** ', data.duration, "data._id ", data._id);
            clearInterval(timerVal1)

            ////////////////////////////////////////////////////////////////////
            let timerVal2 = setInterval(function () {
              triviaGameModel.find({ _id: data._id }, (err, result) => {
                console.log("result[0].startGame", result[0].startGame, "reslut", result);

                if (result && result.length) {
                  if (result[0].startGame == '2') {
                    triviaGameModel.updateOne({ _id: data._id }, { $set: { startGame: 1 } }, { new: true },
                      (err, obj) => {
                        if (obj) {
                          console.log("obj", obj);
                          console.log("startGame change to 1 IN case when no user play game");
                          clearInterval(timerVal2)
                        }
                      })
                  } else {
                    console.log("startGame is already 1 & Finished");
                    clearInterval(timerVal2)
                  }
                }
              })
            }, 3000);
            ////////////////////////////////////////////////////////////////////

          }
        }, 1000);
        //websocket.emit('stopTimer',{started:false})
        console.log('countdown running ***** ', data.duration);
        clearInterval(timerVal)
      }
    }, 1000);
    }


  });

  socket.on('disconnect', () => {
    console.log("start game data===bye");
  });
})












  // gameModel.find({$and:[{startTime:{"$gte": new Date(y,m,day,0,0,0,0)},
  // startTime:{$lte:new Date(y,m,day,23,59,59,999)}}]},function(err,data){
  //   if(err){
  //     console.log("Error in game find query.");
  //   }
  //   else{
  //     console.log("game data=======",data);
  //     if(data.length){
  //       websocket.emit('gameId', { gameId: data[0]._id });
  //       for(let each in data){
  //         if(new Date(data[each].startTime).getTime() == d.getTime()){
  //           countdown = data[each].duration;
  //           console.log("game data matched===",data[each]._id);
  //           websocket.emit('gameId', { gameId: data[each]._id });
  //         }
  //       }
  //     }
  //   }
  //
  // })

  // 	let timerVal = setInterval(function() {
  //
  //   	countdown--;
  //   	websocket.emit('timer', { countdown: countdown,timerStarted:true });
  //   	console.log('timer running ***** ',countdown);
  //   	if(countdown == 0){
  // 		console.log('countdown running ***** ',countdown);
  // 		clearInterval(timerVal)
  // 	}
  // }, 1000);


// websocket.emit('connection', 'Hello World');
// websocket.on('connection', (obj) => {
//   console.log('Object from RN app');
// })


// websocket.on('connection', function (socket) {
//   socket.on('reset', function (data) {
//     countdown = 10;
//     websocket.emit('timer', { countdown: countdown });
//   });
// });
