const globalConstants = {
  //LOCALURL: 'http://tugofwar.mobilytedev.com:8092/',
  LOCALURL: 'http://localhost:8092/',
  STAGEURL: 'http://tugofwar.mobilytedev.com:8092/',
  PRODURL: 'http://tugofwar.mobilytedev.com:8092/',
  LIVEURL: 'http://tugofwar.mobilytedev.com:8092/',
  STAGINGURL: 'http://tugofwar.mobilytedev.com:8092/',

  JWTOKENLOCAL: 'fcv42f62-g465-4dc1-ad2c-sa1f27kk1w43',
  JWTOKENSTAGING: 'fax42c62-g215-4dc1-ad2d-sa1f32kk1w22',
  JWTOKENDEV: 'fcv42f62-g465-4dc1-ad2c-sa1f27kk1w43',
  JWTOKENLIVE: 'asd42e62-g465-4bc1-ae2c-da1f27kk3a20',
  key: {
    privateKey: 'c3f42e68-b461-4bc1-ae2c-da9f27ee3a20',
    tokenExpiry: 1 * 30 * 1000 * 60 * 24 //1 hour
  },
  FROM_MAIL: {
    LOCALHOST: "",
    LIVE: "",
    STAGING: ""
  },
  SMTP_CRED: {
    LOCALHOST: {
      email: 'mobilytesolu123@gmail.com',
      password: '123456789mobilyte'
    },
    LIVE: {},
    STAGING: {}
  },
  firebase: {
    apiKey: "",
    databaseURL: ""
  },
  accountSid: "ACb2fb1d4805decfdfc67b730980aa9739",
  authToken: "be9a47377c55d97eed33bc58260dd56a",
  twilioFrom: "+13146870569",

  // Local
  accountSidLocal: "ACb2fb1d4805decfdfc67b730980aa9739",
  authTokenLocal: "be9a47377c55d97eed33bc58260dd56a",
  twilioFromLocal: "+18327936705",
  countryLocal: "+1",
  countryProd: "+1",

  FCM: {
    SERVERKEY: 'AAAAdOyndbA:APA91bFpet8i1DoeOOSa2ZE-Yx-rQOA4LaEhVSJupCtTZ_IQEeox0HUXt6xT6kXfbB_zyFqyqMwVjwF8QeMOsrlupny51oyC4QHZXCWiqur6ji_83v7Fi4mpmxudAOFStdmq0bDyhAdI',
    LOCALHOST: {
      SERVERKEY: 'AAAAdOyndbA:APA91bFpet8i1DoeOOSa2ZE-Yx-rQOA4LaEhVSJupCtTZ_IQEeox0HUXt6xT6kXfbB_zyFqyqMwVjwF8QeMOsrlupny51oyC4QHZXCWiqur6ji_83v7Fi4mpmxudAOFStdmq0bDyhAdI',
    },
    LIVE: {
      SERVERKEY: 'AAAAdOyndbA:APA91bFpet8i1DoeOOSa2ZE-Yx-rQOA4LaEhVSJupCtTZ_IQEeox0HUXt6xT6kXfbB_zyFqyqMwVjwF8QeMOsrlupny51oyC4QHZXCWiqur6ji_83v7Fi4mpmxudAOFStdmq0bDyhAdI',
    },
    STAGING: {
      SERVERKEY: 'AAAAdOyndbA:APA91bFpet8i1DoeOOSa2ZE-Yx-rQOA4LaEhVSJupCtTZ_IQEeox0HUXt6xT6kXfbB_zyFqyqMwVjwF8QeMOsrlupny51oyC4QHZXCWiqur6ji_83v7Fi4mpmxudAOFStdmq0bDyhAdI',
    },
  },
  
  MONGODB: {
    LOCALHOST: {
    //URL: 'mongodb://tugofwar.mobilytedev.com:27017/tugofwar'
    //  URL: 'http://tugofwar.mobilytedev.com:8092/tugofwar'
       URL: 'mongodb://localhost:27017/tugofwar'
    },
    TEST: {
      URL: 'mongodb://localhost:27017/tugofwar'
    },
    LIVE: {
      URL: 'mongodb://localhost:27017/tugofwar'
    },
    STAGING: {
      URL: 'mongodb://localhost:27017/tugofwar'
    },
  },
};

module.exports = globalConstants;
// Google client - 899638143106-vq7md1h6u2tbc3fus2g1nmt9rt076962.apps.googleusercontent.com
// Google secret - W34TpGDuwXHZunpYAsIULUiD
