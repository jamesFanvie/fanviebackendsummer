const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const crypto = require('crypto');
//let mongooseHidden = require('mongoose-hidden')();
const algorithm = 'aes-256-ctr';

const secretKey = 'fax42c62-g215-4dc1-ad2d-sa1f32kk1w22';

const emailValidator = (email) => {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
};

const userSchema = new Schema({
  select_team: String,
  device_id: String,
  _id: {type: Schema.ObjectId, unique: 1},
  email: { type: String,  default: '' },
  password: { type: String, hide: true },
  status: { type: String, enum: ['active', 'suspended'], default: 'suspended' },
  role: { type: String, enum: ['ADMIN', 'USERS'], default: 'USERS' },
  name: { type: String, default: '' },
  dob: { type: Date },
  description: { type: String, default: '' },
  phone: { type: String, default: '' },
  otp: { type: Number },
  deviceToken: { type: String, default: '' },
  location: { type: String, default: '' },
  gender: { type: String, default: '' },
  socialId: { type: String, default: '' },
  socialType: { type: String, enum: ['web', 'Google', 'Facebook'], default: 'web' },
  description: { type: String, default: '' },
  profile: { type: Schema.ObjectId, ref: 'Docs' },
  otpExpiresAt: { type: Date },

  geometry: {
    coordinates: { type: [] },
    type: { type: 'String', enum: ['Polygon', 'Point'], default: 'Point' },
  },
}, {
    timestamps: true,
  });

userSchema.set('toObject');
userSchema.set('toJSON');
const User = mongoose.model('User', userSchema);
module.exports = User;
