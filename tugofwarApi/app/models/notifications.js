var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
//moment = require('moment');
var notificationSchema = new Schema({
    senderId: [{ type: mongoose.Schema.ObjectId, ref: 'users' }],//This is userId
    receiverId: [{ type: mongoose.Schema.ObjectId, ref: 'users' }],
    message: { type: String },
    title: { type: String },
    body: { type: String }
    //sentBy: { type: String, enum: ["Customer", "ServiceProvider"] },
    //notificationType: { type: String, enum: ["Schedule Changed", "serviceProviderAddition"] },
    //status: { type: String, enum: ["read", "unread"], default: "unread" },
    //   createdAt   : {type : Date,default   : Date.now()}
}, {
        timestamps: true,
    });
module.exports = mongoose.model('notifications', notificationSchema);
