import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserreportComponent } from './userreport/userreport.component';
import { EventComponent } from './event/event.component';
import { EventreportComponent } from './eventreport/eventreport.component';
import { NotificationComponent } from './notification/notification.component';
import { GameListComponent } from './game-list/game-list.component';
import { TriviaGameListComponent } from './triviaGame-list/triviaGame-list.component';
import { AddgameComponent } from './addgame/addgame.component';
import { AddtriviagameComponent } from './addTriviaGame/addTriviaGame.component';
import { RematchComponent } from './rematch/rematch.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  // { path: '*', redirectTo: '/login', pathMatch: 'full' },
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard', component: DashboardComponent,
    children: [
      { path: 'users', component: UserListComponent },
      { path: 'addgame', component: AddgameComponent, canActivate: [AuthGuard] },
      { path: 'addTriviagame', component: AddtriviagameComponent, canActivate: [AuthGuard]},
      { path: 'gamelist', component: GameListComponent, canActivate: [AuthGuard] },
      { path: 'triviaGameList', component: TriviaGameListComponent, canActivate: [AuthGuard] },
      { path: 'editgame/:_id', component: AddgameComponent, canActivate: [AuthGuard] },
      { path: 'analytics', component: AnalyticsComponent, canActivate: [AuthGuard] },
      { path: 'event', component: EventComponent },
      { path: 'eventreport', component: EventreportComponent },
      { path: 'notification', component: NotificationComponent },
      { path: 'rematch', component: RematchComponent }
    ]
  },
  { path: '**', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [AuthService, AuthGuard],
  exports: [RouterModule]
})

export class AppRoutingModule {


}
export const routingComponents = [LoginComponent,
  DashboardComponent,
  GameListComponent,
  UserListComponent,
  UserreportComponent,
  EventComponent,
  EventreportComponent,
  NotificationComponent,
  AnalyticsComponent,
  RematchComponent]
