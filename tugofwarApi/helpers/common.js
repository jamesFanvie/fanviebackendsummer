module.exports = {
  generateNewPassword: function () {
    const numbers = '0123456789';
    const smallCharacters = 'abcdefghijklmnopqrstuvwxyz';
    const bigCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const specialCharacters = '!@#$%^&*()';
    let returnText = '';
    return numbers.charAt(Math.floor(Math.random() * 10)) + smallCharacters.charAt(Math.floor(Math.random() * 26)) + numbers.charAt(Math.floor(Math.random() * 10)) + bigCharacters.charAt(Math.floor(Math.random() * 26)) + smallCharacters.charAt(Math.floor(Math.random() * 26)) + specialCharacters.charAt(Math.floor(Math.random() * 10)) + bigCharacters.charAt(Math.floor(Math.random() * 26)) + numbers.charAt(Math.floor(Math.random() * 10));
  },
  getEmailTemplate: function (referenceId, cb) {
    const TemplateModel = require('../models/template.js');
    TemplateModel
      .findOne(
      {
        "title": referenceId,
        "status": true
      },
      {
        "status": 0,
        "isDeleted": 0,
        "createdDate": 0,
        "modifiedDate": 0
      }
      )
      .exec(
      function (err, template) {
        if (err) {
          cb();
        } else {
          if (template) {
            cb(template);
          } else {
            cb();
          }
        }
      }
      )
  },
  randomIntegerOtp: function() {
    return Math.floor(1000 + Math.random() * 9000);
  },
  getRandomColor: function () {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }

}
