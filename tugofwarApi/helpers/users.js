const uniqueValidator = require('mongoose-unique-validator');
const imgPath = require('path');
const crypto = require('crypto');
const UserModel = require('../app/models/users');
const algorithm = 'aes-256-ctr';
const fs = require('fs')
const env = require('../config/env')();
var request = require('request');
const twilio = require('twilio');
const accountSid = env.accountSid;
const authToken = env.authToken;
const twilioFrom = env.twilioFrom;
var client = new twilio(accountSid, authToken);
const secretKey = 'fax42c62-g215-4dc1-ad2d-sa1f32kk1w22';
// const FCMKey = env.FCM;
// var firebase = require('firebase');
// var FCM = require('fcm-push');
// var fcm = new FCM(FCMKey);

const _ = require("underscore");
const emailValidator = (email) => {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
};
const zipValidator = (zip) => {
  return (/(^\d{6}$)|(^\d{6}-\d{4}$)/).test(zip);
};
function encrypt(text) {
  const cipher = crypto.createCipher(algorithm, secretKey);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}
function decrypt(text) {
  const decipher = crypto.createDecipher(algorithm, secretKey);
  console.log("text===========", text);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  console.log("dec=========", dec);
  return dec;
}
function isDirSync(aPath) {
  try {
    return fs.statSync(aPath).isDirectory();
  } catch (e) {
    if (e.code === 'ENOENT') {
      return false;
    } else {
      throw e;
    }
  }
}
function sendFcm(message, cb) {
  fcm.send(message, function (err, messageId) {
    console.log("=======fcm");
    if (err) {
      cb("Something has gone wrong!", err);
    } else {
      console.log("========Sent with message ID:", messageId);
      cb(null, "Sent with message ID: ", messageId);
    }
  });
}
function sendTwilio(message, cb) {
  console.log("client part===");
  client.messages.create(message, function (err, result) {
    if (error) {
      return cb({ status: 'error', message: 'Error in searching email' });
    }
    return cb({ status: 'success', data: results });
  })
  // .then(function(message1){
  //   cb(message1.sid);
  // }).catch((error)=>{
  //   console.log("error===================",error);
  // });
}

module.exports = {
  getEmailInfo: async (email, phone, cb) => {
    let quer;
    if (email) quer = { "email": email.toLowerCase() };
    if (phone) quer = { phone: phone };
    UserModel.aggregate([
      {
        $match: quer
      },
      {
        $lookup:
        {
          from: "docs",
          localField: 'profile',
          foreignField: '_id',
          as: "Docs"
        }
      }
    ], function (error, results, fields) {
      if (error) {
        return cb({ status: 'error', message: 'Error in searching phone/Email' });
      }
      return cb({ status: 'success', data: results });
    });
  },
  getUserInfo: async (email, phone, cb) => {
    let quer;
    if (email) quer = { "email": email.toLowerCase() };
    if (phone) quer = { phone: phone };
    UserModel.findOneAndUpdate({ quer }, function (error, results, fields) {
      if (error) {
        return cb({ status: 'error', message: 'Error in searching email' });
      }
      return cb({ status: 'success', data: results });
    });
  },
  phoneSms: (otp, phone, cb) => {
    const accountSid = 'AC1ebcc31dfdae49d8badd14f952d940d2';
    const authToken = '2693b3f479f859da05be548685652e9d';
    const client = require('twilio')(accountSid, authToken);
    client.messages
      .create({
        from: '+18327936705 ',
        body: 'new smg',
        to: '+1' + phone,
        //   to:phone
      }, function (err, result) {
        if (err) {
          return cb({ status: 'error', message: 'Error in sending message' });
        }
        else {
          return cb({ status: 'success', message: 'Successfully sent.' });
        }
      })
    // .then(message => console.log(message))
    // .done();
  },
  sendMessage: async (to, body, cb) => {
    console.log("phone verify==========");
    let message = {
      body: body,
      to: to,  // Text this number
      from: twilioFrom // From a valid Twilio number
    }
    console.log("phone verify=====message=====", message);
    //  sendTwilio(message,function(data){
    //    console.log(data,"Message sent");
    //
    //  })
    client.messages.create(message, function (err, result) {
      if (err) {
        return cb({ status: 'error', message: 'Error in searching email' });
      }
      return cb({ status: 'success', data: result });
    })
  },
  getEmailFacebookInfo: async (data, cb) => {
    let quer;
    if (data.email) {
      quer = { $or: [{ "email": data.email.toLowerCase() }] };
      if (data.id) quer["$or"].push({ socialId: data.id });
    } else {
      quer = { socialId: data.id }
    }
    UserModel.aggregate([
      {
        $match: quer
      },
      {
        $lookup:
        {
          from: "docs",
          localField: 'profile',
          foreignField: '_id',
          as: "Docs"
        }
      }
    ], function (error, results, fields) {
      if (error) {
        return cb({ status: 'error', message: 'Error in searching email' });
      }
      return cb({ status: 'success', data: results });
    });
  },
  sendFirebase: async (userId, title, body) => {
    console.log("=sendFirebase===");
    let message = {
      to: userId, // required fill with device token or topics
      notification: {
        title: title,
        body: body
      }
    }
    // if(type) {
    //   message.data={type:type,event:eventData};
    // }
    // if(eventData){
    //     message.data={event:eventData,type:type};
    // }
    console.log("=============ghjghj", message);
    sendFcm(message, function (err, data) {
      console.log("results==============data====", data);
      console.log(err, data);
    })
  },
  generateHashPassword: async (password) => {
    const hash = await encrypt(password);
    return hash;
  },
  comparePassword: (login_password, orignal_password) => {
    const decryptedPassword = decrypt(orignal_password);
    console.log(login_password, "login_password helpers=============", decryptedPassword);
    return login_password == orignal_password;
  },
  emailValidate: (email) => {
    return emailValidator(email);
  },
  zipValidate: (zip) => {
    return zipValidator(zip);
  },
  socialImageSave: async (socialType, profile, id, cb) => {
    if (socialType == "GOOGLE" || socialType == "FACEBOOK") {
      console.log("profile===================", profile);
      if (!profile) return cb({ error: 400, status: 0, messages: 'Please send profile' });;
      request(profile, { encoding: 'binary' }, function (err, response, body) {
        if (err) {
          return cb({ error: 400, status: 0, messages: 'Error in requesting file.', err: err });
        }
        else {
          try {
            var image = profile.split('/');
            var imageLength = image.length;
            var imageName = image[parseInt(imageLength - 1)]
            var pathFile = imgPath.extname(imageName).toLowerCase();
            console.log("pathFile=======", pathFile);
            if (pathFile === '.png' || pathFile === '.jpeg' || pathFile === '.gif' || pathFile === '.jpg') {
              if (!isDirSync("./public/")) {
                fs.mkdirSync("./public");
              }
              if (!isDirSync("./public/upload")) {
                fs.mkdirSync("./public/upload");
              }
              if (!isDirSync("./public/upload/profile")) {
                fs.mkdirSync("./public/upload/profile");
              }
              if (!isDirSync("./public/upload/profile/" + id + "")) {
                fs.mkdirSync("./public/upload/profile/" + id + "");
              }
              fs.writeFile("./public/upload/profile/" + id + "/" + imageName, body, 'binary', function (err) {
                if (err) {
                  return cb({ error: 400, status: 0, messages: 'Error in uploading files.' });
                } else {
                  var imagePath = "upload/profile/" + id + "/" + imageName;
                  return cb({ status: 1, data: imagePath, imageType: pathFile, ImageName: imageName });
                }
              });
            }
            else {
              message = "This format is not allowed , please upload file with '.png','.gif','.jpg','.jpeg'";
              return cb({ status: 0, message: message });
            }
          }
          catch (err) {
            console.log('err=================', err);
            return cb({ error: 400, status: 0, messages: 'Error in try-catch' });
          }
        }
      });
    } else {
      message = "Only singUp Google or Facebook.";
      return cb({ status: 0, message: message });
    }
  },
  saveImage: async (files, id, folder, cb) => {
    try {
      var img_name = files.name;
      console.log("img_name",img_name)
      var path1 = files.path;
      var pathFile = imgPath.extname(img_name).toLowerCase();
      if (pathFile === '.png' || pathFile === '.jpeg' || pathFile === '.gif' || pathFile === '.jpg') {
        fs.readFile(path1, function (error, file_buffer) {
          if (error) {
            return cb({ error: 400, status: 0, messages: 'Error in reading File.' });
          }
          else {
            if (!isDirSync("./public/")) {
              fs.mkdirSync("./public");
            }
            if (!isDirSync("./public/upload")) {
              fs.mkdirSync("./public/upload");
            }
            if (!isDirSync("./public/upload/" + folder + "")) {
              fs.mkdirSync("./public/upload/" + folder + "");
            }
            if (!isDirSync("./public/upload/" + folder + "/" + id + "")) {
              fs.mkdirSync("./public/upload/" + folder + "/" + id + "");
            }
            fs.writeFile("./public/upload/" + folder + "/" + id + "/" + img_name, file_buffer, function (err) {
              if (err) {
                return cb({ error: 400, status: 0, messages: 'Error in uploading files.' });
              } else {
                fs.unlink(path1, function (err, result1) { if (err) console.log("Error in unlinking file") });
                var imagePath = "upload/" + folder + "/" + id + "/" + img_name;
                var imageData = imagePath;
                return cb({ status: 1, data: imageData, imageName: img_name, imageSize: files.size, imageType: files.type });
              }
            });
          }
        });
      }
      else {
        message = "This format is not allowed , please upload file with '.png','.gif','.jpg','.jpeg'";
        return cb({ status: 0, message: message });
      }
    }
    catch (err) {
      return cb({ error: 400, status: 0, messages: 'Error in try-catch' });
    }
  },
}
