export const environment = {
  production: true
};
export const config = {
  local: {
    API_URL: "http://tugofwar.mobilytedev.com:8092/api/",
    SOCKET_URL: "tugofwar.mobilytedev.com:8092",
  },
  dev: {
    API_URL: "http://tugofwar.mobilytedev.com:8092/api/",
  },
  test: {
    API_URL: "http://localhost:8092/api/"
   // API_URL: "http://tugofwar.mobilytedev.com:8092/api/",
  },
  production: {
    API_URL: "http://tugofwar.mobilytedev.com:8092/api/",
  },
  live: {
    API_URL: "http://tugofwar.mobilytedev.com:8092/api/",
  }
}