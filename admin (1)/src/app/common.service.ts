import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { config } from '../../src/environments/environment';
@Injectable({
    providedIn: 'root'
})
export class CommonService {
    constructor(private http: HttpClient) { }
    getGamelist() {
        return this.http.get<any>(`${config.local.API_URL}admin/gameList`, {})
            .pipe(map(game => {
                console.log("game===", game)
                // login successful if there's a jwt token in the response
                if (game && game.status === 1) {
                    console.log("user===", game)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return game;
            }));
    }
    getTriviaGameList() {
          return this.http.get<any>(`${config.local.API_URL}admin/triviaGameList`, {})
            .pipe(map(triviagame => {
                console.log("triviagame===", triviagame)
                // login successful if there's a jwt token in the response
                if (triviagame && triviagame.status === 1) {
                    console.log("user===", triviagame)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return triviagame;
            }));
    }
    getSingleGameData(_id) {
        return this.http.get(`${config.local.API_URL}users/gameDetails/${_id}`, {})
            .pipe(map(game => {
                return game;
            }))
    }
    deleteAddGame(id) {
        return this.http.get<any>(`${config.local.API_URL}admin/deleteAddGame/` + id, {})
            .pipe(map(game => {
                console.log("game===", game)
                // login successful if there's a jwt token in the response
                if (game && game.status === 1) {
                    console.log("user===", game)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return game;
            }));
    }
    //price: string, brand: string,
    //addGame(max, duration: string, location: string, files) {
    addGame(newData) {
        //, priceImage: price, brandLogo: brand 
        return this.http.post<any>(`${config.local.API_URL}admin/addGame`, newData)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }
    addTriviaGame(newData) {
        //, priceImage: price, brandLogo: brand 
        return this.http.post<any>(`${config.local.API_URL}admin/addTriviaGame`, newData)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("usertiriva===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }
    editGame(newData) {
        //, priceImage: price, brandLogo: brand 
        return this.http.post<any>(`${config.local.API_URL}admin/editGame`, newData)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }
    startGame(id: string) {
        return this.http.post<any>(`${config.local.API_URL}admin/startGame`, { gameId: id })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes      
                }
                return user;
            }));
    }
    startTriviaGame(id: string){
        return this.http.post<any>(`${config.local.API_URL}admin/startTriviaGame`, { gameId: id })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes      
                }
                return user;
            }));
    }
    allUsers(){
        return this.http.get(`${config.local.API_URL}admin/allNotificationsUsers`,{})
        .pipe(map(user => {
            return user;
        }))
    }

    sendNotification(newData){
        return this.http.post<any>(`${config.local.API_URL}admin/sendNotification`, newData)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes      
                }
                return user;
            }));
    }
    rematch(newData) {
        //, priceImage: price, brandLogo: brand 
        return this.http.post<any>(`${config.local.API_URL}admin/rematch`, newData)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }

    updateRematch(newData) {
        //, priceImage: price, brandLogo: brand 
        return this.http.post<any>(`${config.local.API_URL}admin/editTeams`, newData)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }

    addTeam(newData) {
        //, priceImage: price, brandLogo: brand 
        return this.http.post<any>(`${config.local.API_URL}admin/addTeam`, { team: newData })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }
    getTeams() {
        return this.http.get<any>(`${config.local.API_URL}admin/getTeams`, {})
            .pipe(map(user => {
                console.log("user===", user)
                // login successful if there's a jwt token in the response
                if (user && user.status === 1) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }

    getRematchUsers() {
        return this.http.get<any>(`${config.local.API_URL}admin/userInRematch`, {})
            .pipe(map(user => {
                return user;
            }));
    }

    getImages() {
        return this.http.get<any>(`${config.local.API_URL}admin/applicationImages`, {})
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.status === 1) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }
    getUser() {
        return this.http.get<any>(`${config.local.API_URL}admin/userlist`, {})
            .pipe(map(user => {
                console.log("user===", user)
                // login successful if there's a jwt token in the response
                if (user && user.status === 1) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }
    getUsersReport() {
        return this.http.get<any>(`${config.local.API_URL}admin/userReportList`, {})
            .pipe(map(user => {
                console.log("user===", user)
                // login successful if there's a jwt token in the response
                if (user && user.status === 1) {
                    console.log("user===", user)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return user;
            }));
    }
    getEventList() {
        return this.http.get<any>(`${config.local.API_URL}admin/eventlist`, {})
            .pipe(map(event => {
                console.log("event===", event)
                // login successful if there's a jwt token in the response
                if (event && event.status === 1) {
                    console.log("event===", event)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return event;
            }));
    }
    getEventReports() {
        return this.http.get<any>(`${config.local.API_URL}admin/channelReportList`, {})
            .pipe(map(eventReport => {
                console.log("eventReport===", eventReport)
                // login successful if there's a jwt token in the response
                if (eventReport && eventReport.status === 1) {
                    console.log("event===", eventReport)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return eventReport;
            }));
    }
    statusChange(id, status) {
        return this.http.post<any>(`${config.local.API_URL}admin/statusChange`, { userId: id, status: status })
            .pipe(map(statusChange => {
                console.log("statusChange===", statusChange)
                // login successful if there's a jwt token in the response
                if (statusChange && statusChange.status === 1) {
                    console.log("statusChange===", statusChange)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return statusChange;
            }));
    }
    deleteUser(id) {
        return this.http.post<any>(`${config.local.API_URL}admin/deleteUser`, { userId: id })
            .pipe(map(deleted => {
                console.log("deleted===", deleted)
                // login successful if there's a jwt token in the response
                if (deleted && deleted.status === 1) {
                    console.log("deleted===", deleted)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return deleted;
            }));
    }
    notificationList() {
        return this.http.get<any>(`${config.local.API_URL}admin/getNotificationByAdmin`, {})
            .pipe(map(notification => {
                console.log("deleted===", notification)
                // login successful if there's a jwt token in the response
                if (notification && notification.notification === 1) {
                    console.log("notification===", notification)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return notification;
            }));
    }
    finishedGame() {
        return this.http.get<any>(`${config.local.API_URL}admin/finishedGame`, {})
            .pipe(map(finishedGame => {
                console.log("deleted===", finishedGame)
                // login successful if there's a jwt token in the response
                if (finishedGame && finishedGame.notification === 1) {
                    console.log("notification===", finishedGame)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                }
                return finishedGame;
            }));
    };
}


