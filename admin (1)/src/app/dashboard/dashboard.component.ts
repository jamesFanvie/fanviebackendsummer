import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router'
import { first } from 'rxjs/operators';
import { CommonService } from '../common.service';
// @Component({
//   selector: 'app-dashboard',
//   templateUrl: './dashboard.component.html',
//   styleUrls: ['./dashboard.component.css']
// })
// export class DashboardComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

//{templateUrl: 'login.component.html',selector: 'login-root'}

@Component({
  templateUrl: 'dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  //@Input () myEvent = new EventEmitter();

  // users=[];
  // Counter = 5;
  constructor(private route: ActivatedRoute, private router: Router,
    private commonService: CommonService
  ) { }

  ngOnInit() {
  }

  refresh() {
    window.location.reload();
  };



}
