import { Component, OnInit } from '@angular/core';
import { DashboardComponent } from '../dashboard/dashboard.component'
import { CommonService } from '../common.service'
import { first } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-analytics',
    templateUrl: './analytics.component.html',
    styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

    users = [];
    p: number = 1;
    closeResult: string;
    showModalData: any;
    showRematchUsers = [];

    constructor(private commonService: CommonService,
        private modalService: NgbModal) { }

    ngOnInit() {
        this.commonService.finishedGame()
            .pipe(first())
            .subscribe(
                data => {
                    this.users = data;
                    console.log("data============", this.users)
                    //this.router.navigate(['/departments',department.id])
                    //this.router.navigate(['/dashboard','users']);
                },
                error => {
                    console.log('error', error)
                });
    }

    open(content, data) {
        console.log(data);
        this.showModalData = data.away;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    open2(content, data) {
        console.log(data);
        this.showModalData = data.home;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    getRematchUsers() {
        this.commonService.getRematchUsers()
            .pipe(first())
            .subscribe(
                data => {
                    //this.router.navigate(['/departments',department.id])
                    console.log("data=========", data.data)
                    this.showRematchUsers = data.data;
                    console.log(this.showRematchUsers)
                },
                error => {
                    console.log('error', error)
                });
    }

}
