const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const rematchSchema = new Schema({
  teamName: { type: String, default: '' },
  userId: { type: Schema.ObjectId, ref: 'Users' },
  gameId: { type: Schema.ObjectId, ref: 'Games' },
  teamId: { type: Schema.ObjectId },
  rematch: { type: String, default: '' },
  userName: { type: String, default: '' },

  gameTime: { type: Date, default: '' },
  location: { type: String, default: '' },
  duration: { type: Number, default: '' },
  playedTeam: { type: String, default: '' },
  winTeam: { type: String, default: '' },
}, { timestamps: true });

rematchSchema.set('toObject');
rematchSchema.set('toJSON');
module.exports = mongoose.model('Rematch', rematchSchema);

 