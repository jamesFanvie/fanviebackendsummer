import { Component, OnInit, NgZone, NgModule, Renderer, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { CommonService } from '../common.service';
import {MatExpansionModule} from '@angular/material/expansion';

export class MyItems {
	Value: string;
	constructor(Value: string) {
		this.Value = Value;
	}
}

@Component({
	selector: 'app-rematch',
	templateUrl: './rematch.component.html',
	styleUrls: ['./rematch.component.css']
})

export class RematchComponent implements OnInit {

	teams: any = [];
	team: string;
	objectId:string;
	homeImageFile: any;
	awayImageFile: any;
	userForm: FormGroup;
	loading = false;
	submitted = false;
	returnUrl: string;
	dateTime: string;
	team1: string;
	team2: string;
	team3: string;
	homeicon: string;
	winnericon: string;
	boxicon: string;
	data: Array<number> = [];
	Value: string;
	updatedItem: string;
	winImage : string;
	homImage: string;
	awyImage: string;
	showRematchUsers = [];

	myItems: MyItems[] = new Array();
	public rematchFile: File;
	// Other variables    
	IsForUpdate: boolean = false;
	newItem: any = {};
	editMode = false;

	constructor(private zone: NgZone, private formBuilder: FormBuilder,
		private commonService: CommonService) {
	}
	@ViewChild(NgForm) myForm: NgForm;

	ngOnInit() {
		this.userForm = this.formBuilder.group({
			dateTime: ['', Validators.required],
			duration: ['', Validators.required],
			team1: ['', Validators.required],
			team2: ['', Validators.required],
			team3: ['', Validators.required],
			homeicon: ['', Validators.required],
			winnericon: ['', Validators.required],
			boxicon: ['', Validators.required],
			Value: ['', Validators.required]
		});
		this.getTeamList();
		// this.getImages();
	}

	// getImages(){
	// 	this.commonService.getImages().subscribe(data => {
	// 		console.log(data);
	// 		this.winImage = data.data[0].path;
	// 		this.homImage = data.data[1].path;
	// 		this.awyImage = data.data[2].path;
	// 		// this.winnerImages = data;
	// 	})
	// }

	removeTeam(index) {
		this.teams.splice(index, 1);
	}

	addTeam() {
		this.commonService.addTeam(this.team).subscribe(data => {
			console.log(data);
			if (data) this.teams = data;
			if (data.status){
				alert(data.message)
				this.getTeamList();
			}			
			else {
				alert(data.message)
			}
		})
	}

	editItem(item) {
		console.log(item);
		this.editMode = true;
		this.team = item.teamName;
		this.objectId = item._id;
	}

	addTeams() {
		if (!this.team) {
			alert("Please enter team");
			return;
		}
		this.commonService.rematch(this.team)
			.pipe(first())
			.subscribe(
				data => {
					//this.router.navigate(['/departments',department.id])
					console.log("data=========", data)
					if (data.status)
						alert(data.message)
					else {
						alert(data.message)
					}
					this.myForm.resetForm();
				},
				error => {
					console.log('error', error)
				});
	}

	updateTeam() {
		if (!this.team) {
			alert("Please enter team");
			return;
		}
		let obj = {
			teamName:this.team,
			id:this.objectId
		}
		this.commonService.updateRematch(obj)
			.pipe(first())
			.subscribe(
				data => {
					//this.router.navigate(['/departments',department.id])
					console.log("data=========", data)
					this.editMode = false;
					if (data.status){
						alert(data.message);
						this.getTeamList();
					}
					else {
						alert(data.message)
					}
				},
				error => {
					console.log('error', error)
				});
	}

	getTeamList() {
		this.commonService.getTeams().subscribe(data => {
			console.log(data);
			if (data)
				this.teams = data.data;
		})
	}

	deleteTeam(id) {
		this.commonService.deleteAddGame(id).subscribe(data => {
			console.log(data);
			if (data.status){
				alert(data.message)
				this.getTeamList();
			}	
		})
	}
	AddItem(team) {
		this.myItems.push(team);
		console.log("test records", this.myItems);
		this.myForm.resetForm();
	}

	// onSubmit() {
	// 	this.loading = true;
	// 	let newData = new FormData();
	// 	var teamsize = this.myItems;
	// 	// for (let i = 0; i < teamsize.length; i++) {
	// 	//     newData.append('team', '' + teamsize[i]);
	// 	// }
	// 	//startTime: max, duration: duration, location: location, files:files
	// 	//newData.append('files', this.priceFile, this.priceFile.name);
	// 	//newData.append('team', this.myItems[]);
	// 	newData.append('winnerImages', this.rematchFile ? this.rematchFile : "");
	// 	newData.append('homeImage', this.homeImageFile ? this.homeImageFile : File);
	// 	newData.append('awayImage', this.awayImageFile ? this.awayImageFile : File);

	// 	//this.commonService.addGame(this.max, this.duration, this.location, _formData)
	// 	this.commonService.rematch(newData)
	// 		.pipe(first())
	// 		.subscribe(
	// 			data => {
	// 				//this.router.navigate(['/departments',department.id])
	// 				console.log("data=========", data)
	// 				if (data.status)
	// 					alert(data.message)
	// 				else {
	// 					alert(data.message)
	// 				}

	// 				this.myForm.resetForm();
	// 			},
	// 			error => {
	// 				console.log('error', error)
	// 			});
	// }

	// public fileEvent($event) {
	// 	this.rematchFile = $event.target.files[0];
	// }
	// public homeEvent($event) {
	// 	this.homeImageFile = $event.target.files[0];
	// }
	// public awayEvent($event) {
	// 	this.awayImageFile = $event.target.files[0];
	// }
	public uploadFile(fileToUpload: File) {
		const _formData = new FormData();
		_formData.append('file', fileToUpload, fileToUpload.name);
		// return <any>post(UrlFileUpload, _formData); //note: no HttpHeaders passed as 3d param to POST!
		//So no Content-Type constructed manually.
		//Angular 4.x-6.x does it automatically.
	}
	//public store(team) {
	//    debugger;
	//    this.myItems.push(team);
	//    console.log("test data", this.myItems)
	//}
	//EditItem(i) {
	//    this.newItem.Value = this.myItems[i].Value;
	//    this.updatedItem = i;
	//    this.IsForUpdate = true;
	//}

	// When user clicks on update button to submit updated value  
	//UpdateItem() {
	//    let data = this.updatedItem;
	//    for (let i = 0; i < this.myItems.length; i++) {
	//        if (i == data) {
	//            this.myItems[i].Value = this.newItem.Value;
	//        }
	//    }
	//    this.IsForUpdate = false;
	//    this.newItem = {};
	//}

	getRematchUsers(){
		this.commonService.getRematchUsers()
			.pipe(first())
			.subscribe(
				data => {
					//this.router.navigate(['/departments',department.id])
					console.log("data=========", data.data)
					this.showRematchUsers = data.data;
				},
				error => {
					console.log('error', error)
				});
	}


}
