import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service'
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: []
})
export class EventComponent implements OnInit {
  eventList = [];
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.commonService.getEventList()
      .pipe(first())
      .subscribe(
        data => {
          this.eventList = data;
          console.log("data============", this.eventList)
          //this.router.navigate(['/departments',department.id])
          //this.router.navigate(['/dashboard','users']);
        },
        error => {
          console.log('error', error)
        });
  }

}
