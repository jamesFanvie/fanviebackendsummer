const userModel = require('../models/users');
const docsModel = require('../models/docs');
const gameModel = require('../models/games');
const usersHelper = require('../../helpers/users');
const io = require('socket.io');
const async = require('async');
const Jwt = require('jsonwebtoken');
const ObjectId = require('objectid');
const mailerHelper = require('../../helpers/mailer');
const commonHelper = require('../../helpers/common')
const env = require('../../config/env')();
const privateKey = 'fcv42f62-g465-4dc1-ad2c-sa1f27kk1w43'
const moment = require('moment');
const rematchModel = require('../models/rematch')
var notificationsModel = require('../models/notifications');
const _ = require('underscore');

// Your new Phone Number is +17637102573
var google = function (req, res) {
  var { id, name, email, picture, city, dob, gender, description, lat, lng } = req.body;
  var files = req.files;
  console.log("get body by google=========", req.body);
  var socialType = 'GOOGLE';
  picture = (picture && picture != "null") ? picture : "";
  const deviceToken = req.body.deviceToken || '';
  if (!usersHelper.emailValidate(email)) return res.json({ status: 0, message: 'Please enter a valid email.' });
  async.waterfall([
    function (callback) {
      usersHelper.getEmailInfo(email, null, function (data) {
        if (data.status == 'error') {
          return res.json({ status: 0, message: 'Error in searching email.' });
        }
        var user = data && data.data && data.data[0] ? data.data[0] : {};
        if (user && !user.socialId) {
          return callback(null, user);
        }
        var token = "";
        delete user.lastActivity;
        delete user.updatedAt;
        delete user.socialId;
        delete user.otp;
        delete user.password;
        var tokenData = {
          name: user.name,
          id: user._id,
          role: user.role
        };
        var token = Jwt.sign(tokenData, privateKey);
        return res.json({ status: 1, message: 'User login successfully.', data: user, token: token });
      });
    },
    function (data, callback) {
      let newUser = new userModel({
        email: email.toLowerCase(),
        name: name,
        dob: dob,
        gender: gender,
        location: city,
        socialId: id,
        deviceToken: deviceToken,
        socialType: 'Google',
        description: description,
      });
      newUser.save(function (err, googleResult) {
        if (err) {
          return res.json({ status: 0, message: 'Error in Registering User.' });
        }
        else {
          let socialType = 'GOOGLE';
          usersHelper.socialImageSave(socialType, picture, googleResult._id, function (imageResult) {
            if (data.statucallbacks == 'error') {
              return res.json({ status: 0, message: 'Error in Image Path.' });
            }
            else {
              let docsNew = new docsModel({
                name: imageResult.ImageName,
                type: imageResult.imageType,
                path: imageResult.data,
                userId: googleResult._id
              });
              docsNew.save(function (err, googleData) {
                if (err) {
                  return res.json({ status: 0, message: 'Error in Registering User image.' });
                }
                else {
                  return callback(null, googleData, googleResult)
                }
              });
            }
          });
        }
      });
    },
    function (googleData, googleResult, callback) {
      userModel.findOneAndUpdate({ _id: googleResult._id }, { $set: { profile: googleData._id } }, { new: true }, function (err, googleImage) {
        if (err) {
          return res.json({ status: 0, message: 'Error in image update.' });
        }
        else {
          var tokenData = {
            name: googleResult.name,
            id: googleResult._id,
            role: googleResult.role
          };
          var token = Jwt.sign(tokenData, privateKey);
          let _id = googleResult._id;
          var newData = JSON.parse(JSON.stringify(googleResult));
          newData['_id'] = tokenData.id;
          if (email) {
            const html = ` Hi,<br><br>
                                               Great to have you in the Eventmatch community!<br>
                                               We hope you will find many cool events and make lots of new friends in here.<br>
                                               If you have questions or any types of comments, feel free to send us an email.<br>
                                               We are always happy to hear from you.<br><br>
                                               Wishing you a great time,<br>
                                               your Eventmatch Crew`;
            const subject = 'Welcome to Eventmatch ';
            mailerHelper.sendMail({ to: email, subject: subject, html: html }, function (err, mailData) {
              if (err) {
                return res.json({ status: 0, message: 'Error in sending mail.' });
              } else {
                return res.json({ status: 1, message: 'User created successfully.', data: user, token: token });
              }
            });
          }
          return res.json({ status: 1, message: 'User registered successfully.', data: newData, token: token });
        }
      });
    }
  ], function (err, googleResult) {
    return res.json({ status: 1, message: 'User registered successfully', users: googleResult });
  });
};
var facebook = function (req, res) {
  var { id, name, email, picture, dob, city, gender, description, lat, lng } = req.body;
  console.log("get body by facebook=========", req.body);
  var files = req.files;
  var socialType = 'FACEBOOK';
  picture = (picture && picture != "null") ? picture : "";
  if (email) {
    email = email.toLowerCase()
    if (!usersHelper.emailValidate(email)) return res.json({ status: 0, message: 'Please enter a valid email.' });
  }
  const deviceToken = req.body.deviceToken || '';
  async.waterfall([
    function (callback) {
      usersHelper.getEmailFacebookInfo({ email: email, id: id }, function (data) {
        if (data.status == 'error') {
          return res.json({ status: 0, message: 'Error in searching FacebookId.' });
        }
        var user = data && data.data && data.data[0] ? data.data[0] : {};
        if (user && !user.socialId) {
          return callback(null, user);
        }
        var token = "";
        delete user.lastActivity;
        delete user.updatedAt;
        delete user.socialId;
        delete user.otp;
        delete user.password;
        var tokenData = {
          name: user.name,
          id: user._id,
          role: user.role
        };
        var token = Jwt.sign(tokenData, privateKey);
        return res.json({ status: 1, message: 'User login successfully.', data: user, token: token });
      });
    },
    function (data, callback) {
      if (!id) return res.json({ status: 'error', message: 'Facebook Id is required.' });
      if (!name) return res.json({ status: 'error', message: 'Name is required.' });
      if (!id) return res.json({ status: 'error', message: 'Email/Id is required.' });
      let newUser = new userModel({
        email: email,
        name: name,
        dob: dob,
        gender: gender,
        deviceToken: deviceToken,
        location: city,
        description: description,
        socialId: id,
        socialType: 'Facebook',
      });
      newUser.save(function (err, facebookResult) {
        if (err) {
          return res.json({ status: 0, message: 'Error in Registering User.' });
        }
        else {
          let socialType = 'FACEBOOK';
          usersHelper.socialImageSave(socialType, picture, facebookResult._id, function (imageResult) {
            if (data.statucallbacks == 'error') {
              return res.json({ status: 0, message: 'Error in Image Path.' });
            }
            else {
              let docsNew = new docsModel({
                name: imageResult.imageName,
                type: imageResult.imageType,
                path: imageResult.data,
                userId: facebookResult._id
              });
              docsNew.save(function (err, facebookData) {
                if (err) {
                  return res.json({ status: 0, message: 'Error in Registering User image.' });
                }
                else {
                  return callback(null, facebookData, facebookResult)
                }
              });
            }
          });
        }
      });
    },
    function (facebookData, facebookResult, callback) {
      userModel.findOneAndUpdate({ _id: facebookResult._id }, { $set: { profile: facebookData._id } }, { new: true }, function (err, googleImage) {
        if (err) {
          return res.json({ status: 0, message: 'Error in image update.' });
        }
        else {
          var tokenData = {
            name: facebookResult.name,
            id: facebookResult._id,
            role: facebookResult.role
          };
          var token = Jwt.sign(tokenData, privateKey);
          var newData = JSON.parse(JSON.stringify(facebookResult));
          newData['_id'] = tokenData.id;
          if (email) {
            const html = ` Hi,<br><br>
                                               Great to have you in the Eventmatch community!<br>
                                               We hope you will find many cool events and make lots of new friends in here.<br>
                                               If you have questions or any types of comments, feel free to send us an email.<br>
                                               We are always happy to hear from you .<br><br>
                                               Wishing you a great time,<br>
                                               your Eventmatch Crew`;
            const subject = 'Welcome to Eventmatch ';
            mailerHelper.sendMail({ to: email, subject: subject, html: html }, function (err, mailData) {
              if (err) {
                return res.json({ status: 0, message: 'Error in sending mail.' });
              } else {
                return res.json({ status: 1, message: 'User created successfully.', data: user, token: token });
              }
            });
          }
          return res.json({ status: 1, message: 'User registered successfully.', data: newData, token: token });
        }
      });
    }
  ], function (err, googleResult) {
    return res.json({ status: 1, message: 'User registered successfully', users: facebookResult });
  });
};
module.exports = {
  signUp: async (req, res) => {
    const { phone, password, rpassword, gender, name, picture } = req.body;
    var files = req.files;
    let folder = "profile";
    console.log("body============", req.body);
    if (!phone) return res.json({ status: 0, message: 'Phone is required.' });
    if (phone.length < 10) return res.json({ status: 0, message: 'Phone should not be less than 10 Number.' });
    if (!password) return res.json({ status: 0, message: 'Password is required.' });
    if (!rpassword) return res.json({ status: 0, message: 'Re-Password is required.' });
    if (!name) return res.json({ status: 0, message: 'Name is required.' });
    var currentDate = new Date();
    var newCurrentDate = new Date(currentDate);
    var randomOtp = commonHelper.randomIntegerOtp();
    newCurrentDate.setMinutes(currentDate.getMinutes() + 30);
    const subject = 'Welcome to Tugofwar ';
    if (password != rpassword) return res.json({ status: 0, message: 'Password & Re-Password should be same.' });
    const deviceToken = req.body.deviceToken || '';
    const newPassword = await usersHelper.generateHashPassword(password);
    usersHelper.getEmailInfo(null, phone, function (data) {
      if (data.status == 'error') {
        return res.json({ status: 0, message: 'Error in searching Phone.' });
      }
      if (data && data.data && data.data.length > 0) {
        return res.json({ status: 0, userStatus: data.data[0].status, message: 'Phone already registered.' });
      }
      let newUser = new userModel({
        phone: phone,
        name: name,
        gender: gender,
        otp: randomOtp,
        otpExpiresAt: newCurrentDate,
        password: newPassword,
        deviceToken: deviceToken,
      });
      newUser.save(function (err, results) {
        if (err) {
          console.log(err)
          return res.json({ status: 0, message: 'Error in inserting user.' });
        }
        else {
          if (files && Object.keys(files).length !== 0) {
            usersHelper.saveImage(files, results._id, function (imageResult) {
              if (imageResult.status == 0) {
                return res.json({ status: 0, message: 'Error in Image Path.' });
              }
              else {
                let docsNew = new docsModel({
                  name: imageResult.imageName,
                  type: imageResult.imageType,
                  path: imageResult.data,
                  size: imageResult.imageSize,
                  userId: results._id
                });
                docsNew.save(function (err, userData) {
                  if (err) {
                    return res.json({ status: 0, message: 'Error in Registering User image.' });
                  }
                  else {
                    userModel.findOneAndUpdate({ _id: results._id }, { $set: { profile: userData._id } }, { new: true }, function (err, googleImage) {
                      if (err) {
                        return res.json({ status: 0, message: 'Error in image update.' });
                      }
                      else {
                        usersHelper.getEmailInfo(null, phone, function (data) {
                          if (data.status == 'error') {
                            return res.json({ status: 0, message: 'Error in searching email after insertion.' });
                          }
                          var user = data.data && data.data[0] ? data.data[0] : {};
                          var token = "";
                          delete user.lastActivity;
                          delete user.updatedAt;
                          delete user.socialId;
                          delete user.otp;
                          delete user.password;
                          var tokenData = {
                            name: user.name,
                            id: user._id,
                            role: user.role
                          };
                          var token = Jwt.sign(tokenData, privateKey)
                          usersHelper.sendMessage(env.country + phone.toString(), "Thank you for signing up to Tugofwar. Your Code is " + randomOtp, function (sendData) {
                            if (sendData.status == 'error') {
                              return res.json({ status: 0, message: 'Error in sending message.' });
                            } else {
                              return res.json({ status: 1, message: 'User created successfully.' });
                            }
                          });
                        });
                      }
                    });
                  }
                });
              }
            });
          }
          else {
            usersHelper.getEmailInfo(null, phone, function (data) {
              if (data.status == 'error') {
                return res.json({ status: 0, message: 'Error in searching email after insertion.' });
              }
              var user = data.data && data.data[0] ? data.data[0] : {};
              var token = "";
              delete user.lastActivity;
              delete user.updatedAt;
              delete user.socialId;
              delete user.otp;
              delete user.password;
              var tokenData = {
                name: user.name,
                id: user._id,
                role: user.role
              };
              var token = Jwt.sign(tokenData, privateKey)
              usersHelper.sendMessage(env.country + phone.toString(), "Thank you for signing up to Tugofwar. Your Code is " + randomOtp, function (sendData) {
                if (sendData.status == 'error') {
                  return res.json({ status: 0, message: 'Error in sending message.' });
                } else {
                  return res.json({ status: 1, message: 'User created successfully.' });
                }
              });
            });
          }
        }
      });
    });
  },
  login: async (req, res) => {
    const { phone, password, type, deviceToken } = req.body;
    console.log("req.body========", req.body);
    if (type == 1) {
      return facebook(req, res);
    }
    if (type == 2) {
      return google(req, res)
    }
    if (!deviceToken) return res.json({ status: 0, message: 'deviceToken is required.' });
    if (!phone) return res.json({ status: 0, message: 'Phone is required.' });
    if (phone.length < 10) return res.json({ status: 0, message: 'Phone should not be less than 10 Number.' });
    if (!password) return res.json({ status: 0, message: 'Password is required.' });
    usersHelper.getEmailInfo(null, phone, function (data) {
      if (data.status == 'error') {
        return res.json({ status: 0, message: 'Error in searching Phone.' });
      }
      if (!data || !data.data || !data.data.length) {
        return res.json({ status: 0, message: 'Phone do not exists.' });
      }
      var user = data.data && data.data[0] ? data.data[0] : {};
      if (user.status == "active") {
        var comparePassword = usersHelper.comparePassword(password, user.password);
        if (!comparePassword) return res.json({
          status: 0, message: "Oops! The PhoneNumber or password doesn't match."
        });
        delete user.lastActivity;
        delete user.updatedAt;
        delete user.password;
        delete user.lastActivity;
        delete user.updatedAt;
        delete user.otp;
        var tokenData = {
          name: user.name,
          id: user._id,
          role: user.role,
        };
        if (tokenData.role == 'ADMIN') {
          var token = Jwt.sign(tokenData, privateKey, {
          });
        }
        else {
          var token = Jwt.sign(tokenData, privateKey);
        }
        userModel.findOneAndUpdate({ "phone": phone }, { deviceToken: deviceToken }, { upsert: true, new: true }, (err, result) => {
          if (err) { return res.json({ status: 0, message: 'some error in query.' }); }
          if (result) {
            return res.json({ status: 1, message: 'User logged in successfully.', data: user, token: token });
          }
          else { return res.json({ status: 0, message: 'some error in query.' }); }
        });
      }
      else {
        return res.json({ status: 0, message: 'User not authorized.' });
      }
    });
  },
  forgotPassword: async (req, res) => {
    const { phone } = req.body;
    if (!phone) return res.json({ status: 0, message: 'Phone no is required.' });
    var currentDate = new Date();
    var newCurrentDate = new Date(currentDate);
    var randomOtp = commonHelper.randomIntegerOtp();
    newCurrentDate.setMinutes(currentDate.getMinutes() + 30);
    usersHelper.getEmailInfo(null, phone, function (data) {
      if (data.status == 'error') {
        return res.json({ status: 0, message: 'Error in searching Phone no.' });
      }
      else {
        userModel.update({ phone: phone }, { $set: { otpExpiresAt: new Date(newCurrentDate), otp: randomOtp, status: "suspended" } }, function (err, updatedData) {
          if (err) {
            return res.json({ status: 0, message: 'some error in query.' });
          }
          else {
            if (data && data.data && data.data.length > 0) {
              usersHelper.sendMessage(env.country + phone.toString(), "Thank you for signing up to Tugofwar. Your Code is " + randomOtp, function (sendData) {
                if (sendData.status == 'error') {
                  return res.json({ status: 0, message: 'Error in sending message.' });
                } else {
                  return res.json({ status: 1, message: 'OTP resend successfully.' });
                }
              });
            }
            else {
              return res.json({ status: 0, message: 'No Phone found.' });
            }
          }
        });
      }
    });
  },
  resetPassword: async (req, res) => {
    const { password, rpassword, phone, otp } = req.body;
    if (!password) return res.json({ status: 0, message: 'Password is required.' });
    if (!rpassword) return res.json({ status: 0, message: 'Re-Password is required.' });
    if (!phone) return res.json({ status: 0, message: 'Phone is required.' });
    if (!otp) return res.json({ status: 0, message: 'Otp is required.' });
    if (password != rpassword) return res.json({ status: 0, message: 'Password & Re-Password should be same.' });
    const newPassword = await usersHelper.generateHashPassword(password);
    async.waterfall([
      function (callback) {
        usersHelper.getEmailInfo(null, phone, function (data) {
          if (data.status == 'error') {
            return res.json({ status: 0, message: 'Error in searching Phone.' });
          }
          else if (data && data.data && data.data.length > 0) {
            if (!data.data[0].otp) {
              return res.json({ status: 0, message: 'No OTP generated.' });
            }
            else if (!data.data[0].otpExpiresAt) {
              return res.json({ status: 0, message: 'No Epirey date for Otp,please regenerate OTP.' })
            }
            else if (new Date(data.data[0].otpExpiresAt).getTime() < (new Date()).getTime()) {
              return res.json({ status: 0, message: 'OTP Expires please regenerate OTP.' })
            }
            else if (data.data[0].otp != otp) {
              return res.json({ status: 0, message: 'OTP do not match with the existing one.' });
            } else {
              callback(null, data);
            }
          }
          else {
            return res.json({ status: 0, message: 'PhoneNumber Nt found.' });
          }
        });
      },
      function (data, callback) {
        userModel.update({ phone: phone }, { $set: { otp: '', password: newPassword, status: 'active' } }, function (err, updatePswd) {
          if (err) {
            return res.json({ status: 0, message: 'Error in updating password.' });
          }
          else {
            callback(null, updatePswd);
          }
        });
      }
    ], function (err, updatePswd) {
      return res.json({ status: 1, message: 'Password updated successfully.' });
    });

  },
  otpMatch: async (req, res) => {
    const { otp, phone } = req.body;
    if (!otp) return res.json({ status: 0, message: 'OTP is required.' });
    if (!phone) return res.json({ status: 0, message: 'Phone is required.' });
    usersHelper.getEmailInfo(null, phone, function (data) {
      if (data.status == 'error') { return res.json({ status: 0, message: 'Error in searching email.' }); }
      else if (data && data.data && data.data.length > 0) {
        if (!data.data[0].otp) { return res.json({ status: 0, message: 'No OTP generated.' }); }
        else if (data.data[0].otpExpiresAt) {
          var date = data.data[0].otpExpiresAt;
          let curentDate = new Date();
          if (new Date(date).getTime() < curentDate.getTime())
            return res.json({ status: 0, message: 'OTP Expires please regenerate OTP.' })
          else {
            if (data.data[0].otp != otp)
              return res.json({ status: 0, message: 'OTP do not match with the existing one.' });
            else {
              userModel.findOneAndUpdate({ phone: phone }, { $set: { status: "active" } },
                { upsert: true, new: true }, function (err, doc) {
                  if (err) {
                    return res.json({ status: 0, message: 'Error in finding phone No.' });
                  }
                  else {
                    return res.json({ status: 1, message: 'OTP matched successfully', data: doc });
                  }
                })
            }
          }
        }
        else {
          return res.json({ status: 0, message: 'No expiry on OTP' });
        }
      }
      else {
        return res.json({ status: 0, message: 'Phone Number Not found.' });
      }
    });
  },
  resend: async (req, res) => {
    const { phone } = req.body;
    if (!phone) return res.json({ status: 0, message: 'Phone no is required.' });
    var currentDate = new Date();
    var newCurrentDate = new Date(currentDate);
    var randomOtp = commonHelper.randomIntegerOtp();
    newCurrentDate.setMinutes(currentDate.getMinutes() + 30);
    usersHelper.getEmailInfo(null, phone, function (data) {
      if (data.status == 'error') {
        return res.json({ status: 0, message: 'Error in searching Phone no.' });
      }
      else {
        userModel.update({ phone: phone }, { $set: { otpExpiresAt: new Date(newCurrentDate), otp: randomOtp, status: "suspended" } }, function (err, updatedData) {
          if (err) {
            return res.json({ status: 0, message: 'some error in query.' });
          }
          else {
            if (data && data.data && data.data.length > 0) {
              usersHelper.sendMessage(env.country + phone.toString(), "Thank you for signing up to Tugofwar. Your Code is " + randomOtp, function (sendData) {
                if (sendData.status == 'error') {
                  return res.json({ status: 0, message: 'Error in sending message.' });
                } else {
                  return res.json({ status: 1, message: 'OTP resend successfully.' });
                }
              });
            }
            else {
              return res.json({ status: 0, message: 'No Phone found.' });
            }
          }
        });
      }
    });
  },
  changePassword: async (req, res) => {
    const { oldPassword, password, rpassword } = req.body;
    if (!oldPassword) return res.json({ status: 0, message: 'Old Password is required.' });
    if (!password) return res.json({ status: 0, message: 'New Password is required.' });
    if (!rpassword) return res.json({ status: 0, message: 'Re-Password is required.' });
    const changedPassword = await usersHelper.generateHashPassword(password);
    if (password != rpassword) return res.json({ status: 0, message: 'Password & Re-Password should be same.' });
    let decoded = req.token;
    userModel.find({ _id: decoded.id }, function (err, userResult) {
      if (err) {
        return res.json({ status: 0, message: 'Error in fetching user details.' });
      }
      else {
        if (userResult && userResult.length) {
          var comparePassword = usersHelper.comparePassword(oldPassword, userResult[0].password);
          if (!comparePassword) {
            return res.json({ status: 0, message: 'Oops! The password you enter doesn\'t match.' });
          }
          else {
            userModel.update({ _id: decoded.id }, { $set: { password: changedPassword } }, function (err, results) {
              if (err) {
                return res.json({ status: 0, message: 'Error in updating password.' });
              }
              else {
                return res.json({ status: 1, message: 'Password updated successfully.' });
              }
            });
          }
        }
        else {
          return res.json({ status: 0, message: 'User is invalid.' });
        }
      }
    });
  },
  gameDetails: async (req, res) => {
    console.log("*********gameDetails*******gameDetails******** gameDetails******gameDetails******");
    let gameId = req.params.id;
    console.log("gameId==================", gameId);
    if (!ObjectId.isValid(gameId)) return res.json({ status: 0, message: 'Game id is not valid' })
    let docData;
    docsModel.find({ imageName: { $in: ['homeImage', 'winnerImages', 'awayImage'] } }, function (err, data) {
      if (err) return res.json({ status: 0, message: 'Error in docs data' });
      else {
        docData = data
        gameModel.aggregate([{ $match: { _id: ObjectId(gameId) } },
        {
          $lookup:
          {
            from: "docs",
            localField: 'brandLogo',
            foreignField: '_id',
            as: "brandLogo"
          }
        },
        { $unwind: { "path": "$brandLogo", "preserveNullAndEmptyArrays": true } },
        {
          $project: {
            startGame: 1,
            geometry: 1,
            date: 1,
            startTime: 1,
            location: 1,
            duration: 1,
            createdAt: 1,
            priceImg: 1,
            numberOfHome: { $size: "$home" },
            numberOfAway: { $size: "$away" },
            brandLogo: { $concat: ["http://tugofwar.mobilytedev.com/", "$brandLogo.path"] },
            _id: 1
          }
        },
        {
          $lookup:
          {
            from: "docs",
            localField: 'priceImg',
            foreignField: '_id',
            as: "priceImg"
          }
        },
        { $unwind: { "path": "$priceImg", "preserveNullAndEmptyArrays": true } },
        {
          $project: {
            startGame: 1,
            geometry: 1,
            date: 1,
            startTime: 1,
            location: 1,
            duration: 1,
            createdAt: 1,
            homeImage: 1,
            awayImage: 1,
            winnerImages: 1,
            numberOfHome: 1,
            numberOfAway: 1,
            brandLogo: 1,
            priceImg: { $concat: ["http://tugofwar.mobilytedev.com/", "$priceImg.path"] }
          }
        },
        { $sort: { startTime: 1 } }

        ], async function (err, data) {
          if (err) {
            return res.json({ status: 0, message: 'Error in find query', err: err });
          }
          else {
            let time = {}
            if (data.length > 0) {
              for (let each in data) {
                let d = data[each]['startTime']
                let winTeam = {};
                if (docData && docData.length > 0) {
                  if (docData && docData[0].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                  if (docData && docData[0].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                  if (docData && docData[0].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                }
                data[each]['time'] = await moment(d).format("hh:mm A");
                if (data[each]['numberOfHome'] > data[each]['numberOfAway']) data[each]['winTeam'] = 'Home';
                if (data[each]['numberOfHome'] < data[each]['numberOfAway']) data[each]['winTeam'] = 'Away';
                if (data[each]['numberOfHome'] == data[each]['numberOfAway']) {
                  if (data[each]['numberOfHome'] == 0 && data[each]['numberOfAway'] == 0) {
                    data[each]['winTeam'] = 'Pendding'
                  }
                  else {
                    if (data[each]['numberOfHome'] == data[each]['numberOfAway']) data[each]['winTeam'] = 'Tie'
                  }
                }
              }
              return res.json({ status: 1, data: data })
            }
            else {
              return res.json({ status: 1, data: data })
            }
          }
        })
      }
    });

  },
  totalCount: async (req, res) => {
    console.log("*********totalCount*******totalCount******** totalCount******totalCount******");
    let { gameId, team, userId, noOfCount } = req.body;
    if (!ObjectId.isValid(gameId)) return res.json({ status: 0, messages: 'Game id is not valid' });
    if (!ObjectId.isValid(userId)) return res.json({ status: 0, messages: 'User id is not valid' })
    if (!noOfCount) return res.json({ status: 0, messages: 'No of couont is required' })
    if (!team) return res.json({ status: 0, messages: 'Team name is required' });
    let select = {
      _id: ObjectId(gameId)
    };
    let up = {
      '$inc': {
      }
    };
    if (team == 'home') {
      select['home.userId'] = ObjectId(userId);
      up['$inc']['home.$.totalCount'] = noOfCount
    }
    else {
      select['away.userId'] = ObjectId(userId);
      up['$inc']['away.$.totalCount'] = noOfCount
    }
    up['startGame'] = 1
    gameModel.update(select, up, function (err, data) {
      if (err) {
        return res.json({ status: 0, messages: 'Error in upadte count query' })
      }
      else {
        return res.json({ status: 1, messages: 'successfully count' })
      }
    })
  },
  create: (req, res) => {
    // Validate request
    if (!req.body.select_team) {
      return res.status(400).send({
        message: "Please select your team"
      });
    }
    const tugOfWar = new userModel({
      select_team: req.body.select_team || "No selection",
      device_id: req.body.device_id || "No selection"
    });
    tugOfWar.save()
      .then(data => {
        res.send({ data: data, statusCode: res.statusCode });
      }).catch(err => {
        res.status(500).send({
          message: err.message || "Some error occurred while selecting team"
        });
      });
  },
  findOne: async (req, res) => {
    //const tugOfWar
    console.log('requseted params ******** ', req.params.device_id)
    userModel.findOne({ device_id: req.params.device_id })
      .then(data => {
        if (!data) {
          return res.status(404).send({
            message: "device not found with id " + req.params.device_id
          });
        }
        if (data.device_id) {
          res.send(data);
        }
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: "device not found with id " + req.params.device_id
          });
        }
        return res.status(500).send({
          message: "Error retrieving data with id " + req.params.device_id
        });
      });
  },
  allUsers: async (req, res) => {
    userModel.find({ role: "USERS" })
      .then(data => {
        res.send(data);
      }).catch(err => {
        res.status(500).send({
          message: err.message || "Some error occurred while retrieving data."
        });
      });
  },
  joinInTeam: async (req, res) => {
    console.log("*********joinInTeam*******joinInTeam******** joinInTeam******joinInTeam******");
    const { userId, select_team, gameId } = req.body;
    if (!ObjectId.isValid(userId)) return res.json({ status: 0, message: 'userId is not valid' });
    if (!select_team) return res.json({ status: 0, message: 'select_team is required' });
    if (!gameId) return res.json({ status: 0, message: 'Game id is required' })
    let commonGroup = {};
    if (!ObjectId.isValid(gameId)) return res.json({ status: 0, message: 'Game id is not valid' })
    let up = {};
    up["$push"] = {};
    if (select_team == 'home')
      up["$push"]["home"] = { "userId": ObjectId(userId) };
    else
      up["$push"]["away"] = { "userId": ObjectId(userId) };
    gameModel.updateOne({ _id: ObjectId(gameId) }, up, function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'Error in add users query.', err: err })
      }
      else {
        return res.json({ status: 1, data: data, message: 'add successfully' })
      }
    });
  },
  winningTeam: async (req, res) => {
    console.log("*********winningTeam*******winningTeam******** winningTeam******winningTeam******");
    if (!req.params.gameId) return res.json({ status: 0, message: 'Game id is requred.' })
    if (!ObjectId.isValid(req.params.gameId)) return res.json({ status: 0, message: 'Game id is not valid' })
    gameModel.aggregate([{ $match: { _id: ObjectId(req.params.gameId) } }, {
      $project: {
        geometry: 1,
        date: 1,
        startTime: 1,
        location: 1,
        duration: 1,
        home: 1,
        away: 1
      }
    }
    ], function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'Error in winner query' })
      }
      else {
        let hometeamcounttotal = 0, awayteamtotal = 0;
        if (data.length > 0) {
          for (let each in data) {
            let winTeam = {};
            console.log("hello")
            for (let value of data[0].away) {
              awayteamtotal = awayteamtotal + value.totalCount
            }
            for (let value of data[0].home) {
              hometeamcounttotal = hometeamcounttotal + value.totalCount
            }
            data[each]['numberOfHome'] = hometeamcounttotal;
            data[each]['numberOfAway'] = awayteamtotal;
            if (data[each]['numberOfHome'] > data[each]['numberOfAway']) data[each]['winTeam'] = 'Home';
            if (data[each]['numberOfHome'] < data[each]['numberOfAway']) data[each]['winTeam'] = 'Away';
            if (data[each]['numberOfHome'] > 0 && data[each]['numberOfAway'] == null) data[each]['winTeam'] = 'Home';
            if (data[each]['numberOfHome'] == null && data[each]['numberOfAway'] > 0) data[each]['winTeam'] = 'Away';
            if (data[each]['numberOfHome'] == 0 && data[each]['numberOfAway'] == null) data[each]['winTeam'] = 'Tie';
            if (data[each]['numberOfHome'] == null && data[each]['numberOfAway'] == 0) data[each]['winTeam'] = 'Tie';
            if (data[each]['numberOfHome'] == data[each]['numberOfAway']) {
              if (data[each]['numberOfHome'] == 0 && data[each]['numberOfAway'] == 0) {
                data[each]['winTeam'] = 'Tie'
              }
              else {
                if (data[each]['numberOfHome'] == data[each]['numberOfAway']) data[each]['winTeam'] = 'Tie'
              }
            }
          }
          gameModel.findOneAndUpdate({ _id: req.params.gameId },
            { "$set": { winTeam: data[0].winTeam } }, { upsert: true }, (err, upddata) => {
              if (err) { return res.json({ status: 0, message: 'Error in gameModel', err: err }); }
              else {
                return res.json({ status: 1, data: data })
              }
            })
        }
      }
    })
  },
  logoutUser: async (req, res) => {
    let { id } = req.body;
    if (!ObjectId.isValid(id)) return res.json({ status: 0, message: 'id is not valid' })
    userModel.findOneAndUpdate({ _id: ObjectId(id) }, { $set: { deviceToken: "" } }, { new: true }, function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'Error in update query' })
      }
      else {
        return res.json({ status: 1, message: 'Successfully logout, deviceToken removed', data: data })
      }
    })
  },
  userRematchTeam: async (req, res) => {
    console.log("req.body -=-=- ", req.body);
    const { teamId, userId, userName, gameId, playedTeam } = req.body;
    if (!userName) return res.json(({ status: 0, message: 'userName is required.' }));
    if (!ObjectId.isValid(teamId)) return res.json(({ status: 0, message: 'teamId is required.' }));
    if (!ObjectId.isValid(userId)) return res.json(({ status: 0, message: 'userId is required.' }));
    if (!ObjectId.isValid(gameId)) return res.json(({ status: 0, message: 'gameId is required.' }));
    if (!playedTeam) return res.json(({ status: 0, message: 'playedTeam is required.' }));
    rematchModel.find({ "_id": ObjectId(teamId) }, function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'error in team find query' })
      }
      if (data.length > 0) {
        gameModel.find({ "_id": ObjectId(gameId) }, function (err, gamedata) {
          if (err) { return res.json({ status: 0, message: 'error in team find query' }) }
          if (gamedata && gamedata.length > 0) {
            let teamNew = new rematchModel({
              userId: userId,
              teamId: teamId,
              userName: userName,
              teamName: data[0].teamName,
              rematch: 1,
              location: gamedata[0].location,
              gameTime: gamedata[0].startTime,
              winTeam: gamedata[0].winTeam,
              duration: gamedata[0].duration,
              playedTeam: playedTeam,
              gameId: gameId
            });
            teamNew.save(async function (err, results) {
              if (err) { return res.json({ status: 0, messages: 'Error in team save query.' }) }
              else { return res.json({ status: 1, message: 'successfully RematchTeam' }) }
            });
          } else { return res.json({ status: 0, message: 'error in rematch team' }) }
        })
      }
      else { return res.json({ status: 0, message: 'error in rematch team' }) }
    })
  },
  userNotificationList: async (req, res) => {
    notificationsModel.find({}, function (err, data) {
      if (err) { return res.json({ status: 0, message: 'error in team find query' }) }
      if (data.length < 1) { return res.json({ status: 0, message: 'No Data Found' }) }
      else { return res.json({ status: 1, NoofNotications: data.length, data: data }) }
    })
  },
  logoImages: async (req, res) => {
    docsModel.find({
      $or: [{ imageName: 'homeImage' }, { imageName: 'winnerImages' }, { imageName: 'awayImage' }]
    }, function (err, data) {
      if (err) { return res.json({ status: 0, message: 'error in  finding Images' }) }
      if (data && data.length < 1) { return res.json({ status: 0, message: 'No Data Found' }) }
      else {
        let obj = {}
        for (let each in data) {
          if (data && data[each].imageName == 'homeImage') obj.homeImage = "http://tugofwar.mobilytedev.com/" + "" + data[each].path;
          if (data && data[each].imageName == 'winnerImages') obj.winnerImages = "http://tugofwar.mobilytedev.com/" + "" + data[each].path;
          if (data && data[each].imageName == 'awayImage') obj.awayImage = "http://tugofwar.mobilytedev.com/" + "" + data[each].path;
        }
        return res.json({ status: 1, data: data.length, data: obj })
      }
    })
  }

};




