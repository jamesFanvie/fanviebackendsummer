const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.set('debug', true);
const gameSchema = new Schema({

  date: { type: Date, default: Date.now() },
  startTime: { type: Date, default: Date.now() },
  endTime: { type: String, default: '' },
  location: { type: String, default: '' },
  duration: { type: Number, default: 0 },
  gametype: { type: String, deflaut: ''},
  gameStatus:{type:String, enum: ['Done', 'Pending'], default: 'Pending'},
  startGame: { type: Number, enum: [0, 1 , 2], default: 0 },
  brandLogo: { type: Schema.ObjectId, ref: 'Docs' },
  priceImg: { type: Schema.ObjectId, ref: 'Docs' },
  geometry: {
    coordinates: { type: [] },
    type: { type: 'String', enum: ['Polygon', 'Point'], default: 'Point' },
  },
  home: [{ userId: { type: Schema.ObjectId, ref: 'Users' }, totalCount: { type: Number, default: 0 } }],
  away: [{ userId: { type: Schema.ObjectId, ref: 'Users' }, totalCount: { type: Number, default: 0 } }],

  homeImage: { type: String, default: '' },
  awayImage: { type: String, default: '' },
  winnerImages: { type: String, default: '' },
  winTeam: { type: String, default: '' },
  
}, {
    timestamps: true,
  });


gameSchema.set('toObject');
gameSchema.set('toJSON');

const Game = mongoose.model('Game', gameSchema);

module.exports = Game;
