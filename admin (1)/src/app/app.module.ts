import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CalendarModule } from 'primeng/calendar';
import { AccordionModule } from 'primeng/accordion';     //accordion and accordion tab
import { MenuItem } from 'primeng/api';                 //api
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helper/jwt';
import { FormsModule, ReactiveFormsModule, NgForm } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserreportComponent } from './userreport/userreport.component';
import { EventComponent } from './event/event.component';
import { EventreportComponent } from './eventreport/eventreport.component';
import { NotificationComponent } from './notification/notification.component';
import { GameListComponent } from './game-list/game-list.component';
import { TriviaGameListComponent } from './triviaGame-list/triviaGame-list.component';
import { AddgameComponent } from './addgame/addgame.component';
import { AddtriviagameComponent } from './addTriviaGame/addTriviaGame.component';
import { GooglePlacesDirective } from './google-places.directive';
import { RematchComponent } from './rematch/rematch.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AnalyticsComponent } from './analytics/analytics.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NumberOnlyDirective } from './number.directive';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {MatInputModule ,MatFormFieldModule,MatExpansionModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    UserListComponent,
    UserreportComponent,
    EventComponent,
    EventreportComponent,

    NotificationComponent,
    GameListComponent,
    TriviaGameListComponent,
    AddgameComponent,
    AddtriviagameComponent,
    RematchComponent,
    AnalyticsComponent,
    NumberOnlyDirective,
    GooglePlacesDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxPaginationModule,
    CalendarModule,
    AngularFontAwesomeModule,
    NgbModule,
    MatExpansionModule,
    MatInputModule,
    // MatButtonModule,
    // MatCheckboxModule,
    MatFormFieldModule,
    BsDatepickerModule.forRoot()
  ],

  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
  bootstrap: [AppComponent]

})
export class AppModule { }
