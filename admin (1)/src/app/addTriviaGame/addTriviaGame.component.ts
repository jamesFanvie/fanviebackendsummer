import {
  Component,
  OnInit,
  NgZone,
  NgModule,
  Renderer,
  ElementRef,
  ViewChild,
  ChangeDetectorRef
} from "@angular/core";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators, NgForm } from "@angular/forms";
import { first } from "rxjs/operators";
import { CommonService } from "../common.service";

declare let google: any;
@Component({
  selector: "app-addtriviagame",
  templateUrl: "./addTriviaGame.component.html",
  styleUrls: ["./addTriviaGame.component.css"]
})
export class AddtriviagameComponent implements OnInit {
  public title = "Places";
  public addrKeys: string[];
  public addr: object;
  public brandFile: File;
  public priceFile: File;
  userForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  //dateTime:string;
  location: string;
  brandLogoShow: string;
  priceImageLogo: string;
  duration: string;
  gametype: string;
  numberofquestion: string;
  price: string;
  brand: string;
  dateTime: string;
  startTime: any;
  city: string;
  lat: any;
  lng: any;
  currentCity: string;
  updateButton: boolean = false;
  gameId: string;
  public rematchFile: File;
  homeImageFile: any;
  awayImageFile: any;

  winImage: string;
  homImage: string;
  awyImage: string;

  public min = new Date();

  public files = { brandLogo: this.brand, priceImage: this.price };

  public max =
    this.min.getUTCFullYear() +
    " " +
    (this.min.getMonth() + 1) +
    " " +
    this.min.getDate() +
    " " +
    this.min.getHours() +
    " " +
    this.min.getMinutes();

  @ViewChild("searchAddress")
  public searchElementRef: ElementRef;

  //private ngZone: NgZone;
  //Method to be invoked everytime we receive a new instance
  //of the address object from the onSelect event emitter.
  //setAddress(addrObj) {
  //    debugger;
  //    //We are wrapping this in a NgZone to reflect the changes
  //    //to the object in the DOM.
  //    this.zone.run(() => {
  //        this.addr = addrObj;
  //        this.addrKeys = Object.keys(addrObj);
  //    });
  //}

  constructor(
    private ngZone: NgZone,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private activeRoute: ActivatedRoute
  ) {}

  @ViewChild(NgForm) myForm: NgForm;

  ngOnInit() {
    console.log("activated route", this.activeRoute.snapshot.params._id);
    if (this.activeRoute.snapshot.params._id) {
      this.callEditGameApiData(this.activeRoute.snapshot.params._id);
    }
    this.startTime = "";
    this.userForm = this.formBuilder.group({
      startTime: ["", Validators.required],
      dateTime: ["", Validators.required],
      duration: ["", Validators.required, Validators.minLength(3)],
      numberofquestion: ["", Validators.required],
      gametype: ["", Validators.required],
      location: ["", Validators.required],
      brand: ["", Validators.required],
      price: ["", Validators.required]
    });
    this.getCurrentAddress();
    this.getImages();
  }

  getImages() {
    this.commonService.getImages().subscribe(data => {
      console.log(data);
      this.winImage = data.data[0].path;
      this.homImage = data.data[1].path;
      this.awyImage = data.data[2].path;
      // this.winnerImages = data;
    });
  }

  callEditGameApiData(_id) {
    this.updateButton = true;
    this.commonService
      .getSingleGameData(_id)
      .pipe(first())
      .subscribe(
        mydata => {
          //this.router.navigate(['/departments',department.id])
          console.log("data=========edit", mydata);
          let requestResponse = JSON.parse(JSON.stringify(mydata));
          console.log("logo issue", requestResponse.data[0].brandLogo);
          this.startTime = requestResponse.data[0].startTime;
          this.duration = requestResponse.data[0].duration;
          this.location = requestResponse.data[0].location;
          this.brandLogoShow = requestResponse.data[0].brandLogo
            ? requestResponse.data[0].brandLogo
            : null;
          this.priceImageLogo = requestResponse.data[0].priceImg
            ? requestResponse.data[0].priceImg
            : null;
          this.gameId = requestResponse.data[0]._id;
        },
        error => {
          console.log("error", error);
        }
      );
  }
  onSubmit() {
    if (this.updateButton) {
      this.loading = true;
      let newData = new FormData();
      newData.append(
        "startTime",
        this.startTime ? new Date(this.startTime).toISOString() : ""
      );
      newData.append("duration", this.duration ? this.duration : "");
      newData.append("numberofquestion", this.numberofquestion ? this.numberofquestion : "");
      newData.append("location", this.location ? this.location : "");
      newData.append("gametype", this.gametype ? this.gametype : "");
      newData.append("lat", this.lat ? this.lat : "");
      newData.append("lng", this.lng ? this.lng : "");
      newData.append("brandLogo", this.brandFile ? this.brandFile : undefined);
      newData.append("priceImage", this.priceFile ? this.priceFile : undefined);
      newData.append("gameId", this.gameId ? this.gameId : undefined);

      this.commonService
        .editGame(newData)
        .pipe(first())
        .subscribe(
          data => {
            //this.router.navigate(['/departments',department.id])
            console.log("data=========", data);
            if (data.status) {
              alert(data.message);
            } else {
              alert(data.message);
            }
            this.myForm.resetForm();
          },
          error => {
            console.log("error", error);
          }
        );
    }
    //if (this.userForm.invalid) {
    //    return;
    //}
    else {
      console.log("======", this.duration);
      //, this.price, this.brand,
      this.loading = true;
      // this.startTime = this.min;
      console.log("startDate=============", this.startTime);
      console.log("numberofQuestion=======", this.numberofquestion);
      let newData = new FormData();
      //startTime: max, duration: duration, location: location, files:files
      //newData.append('files', this.priceFile, this.priceFile.name);
      newData.append(
        "startTime",
        this.startTime ? this.startTime.toISOString() : ""
      );
      newData.append("duration", this.duration ? this.duration : "");
      newData.append("numberofquestion", this.numberofquestion ? this.numberofquestion : "");
      newData.append("location", this.location ? this.location : "");
      newData.append("gametype", this.gametype ? this.gametype : "");
      newData.append("lat", this.lat ? this.lat : "");
      newData.append("lng", this.lng ? this.lng : "");
      newData.append("brandLogo", this.brandFile ? this.brandFile : "");
      newData.append("priceImage", this.priceFile ? this.priceFile : "");

      //this.commonService.addTriviaGame(this.max, this.duration, this.location, _formData)
      this.commonService
        .addTriviaGame(newData)
        .pipe(first())
        .subscribe(
          data => {
            //this.router.navigate(['/departments',department.id])
            console.log("data=========", data);
            if (data.status) {
              alert(data.message);
            } else {
              alert(data.message);
            }
            this.myForm.resetForm();
          },
          error => {
            console.log("error", error);
          }
        );
    }
  }

  public fileEvent($event) {
    this.brandFile = $event.target.files[0];
  }

  public fileEvent2($event) {
    this.priceFile = $event.target.files[0];
  }

  public uploadFile(fileToUpload: File) {
    const _formData = new FormData();
    _formData.append("file", fileToUpload, fileToUpload.name);
    // return <any>post(UrlFileUpload, _formData); //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.x-6.x does it automatically.
  }

  search(form: NgForm) {
    if (!this.location) {
      alert("City is required for search");
      //this.showAlert("City is required")
      return false;
    }
    localStorage.setItem("currentlocation", this.location);
    //localStorage.setItem("selectedCategories", JSON.stringify(this.selectedItems));
    //this.router.navigateByUrl("/Search");
  }

  getCurrentAddress() {
    const autocomplete = new google.maps.places.Autocomplete(
      this.searchElementRef.nativeElement,
      {
        types: ["(cities)"]
      }
    );
    // autocomplete.setComponentRestrictions({
    //   country: [this.googleAutoCompleteCountry]
    // });
    autocomplete.addListener("place_changed", () => {
      debugger;
      this.ngZone.run(() => {
        // get the place result
        const place = autocomplete.getPlace();
        console.log("------", place);
        place.address_components.forEach(val => {
          if (val.types[0] === "locality") {
            this.currentCity = val.long_name;
            this.city = place.formatted_address;
            localStorage.setItem("currentCity", this.currentCity);
          }
        }); // verify result
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        //  this.isAddressValid = true;
        this.lat = place.geometry.location.lat();
        this.lng = place.geometry.location.lng();
        this.location = place.formatted_address;
        console.log("location=================", this.location);
        localStorage.setItem("lat", this.lat.toString());
        localStorage.setItem("long", this.lng.toString());
      });
    });
  }

 
  // Upload winner images etc code

  onSubmit2() {
    this.loading = true;
    let newData = new FormData();
    newData.append("winnerImages", this.rematchFile ? this.rematchFile : "");
    newData.append("homeImage", this.homeImageFile ? this.homeImageFile : File);
    newData.append("awayImage", this.awayImageFile ? this.awayImageFile : File);

    //this.commonService.addGame(this.max, this.duration, this.location, _formData)
    this.commonService
      .rematch(newData)
      .pipe(first())
      .subscribe(
        data => {
          //this.router.navigate(['/departments',department.id])
          console.log("data=========", data);
          if (data.status){
            alert(data.message);
            this.getImages();  
          } 
          else {
            alert(data.message);
          }

          this.myForm.resetForm();
        },
        error => {
          console.log("error", error);
        }
      );
  }


  public fileEven($event) {
    this.rematchFile = $event.target.files[0];
  }
  public homeEvent($event) {
    this.homeImageFile = $event.target.files[0];
  }
  public awayEvent($event) {
    this.awayImageFile = $event.target.files[0];
  }
  public uploadFil(fileToUpload: File) {
    const _formData = new FormData();
    _formData.append("file", fileToUpload, fileToUpload.name);
    // return <any>post(UrlFileUpload, _formData); //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.x-6.x does it automatically.
  }
}