const userModel = require('../models/users');
const usersHelper = require('../../helpers/users');
const gameModel = require('../models/games');
const tirivaGameModel = require('../models/triviaGames');
const docsModel = require('../models/docs')
const rematchModel = require('../models/rematch')
const Jwt = require('jsonwebtoken');
const env = require('../../config/env')();
const privateKey = 'fcv42f62-g465-4dc1-ad2c-sa1f27kk1w43'
const ObjectId = require('objectid');
const moment = require('moment');
const async = require('async');
const _ = require('underscore');
var notificationsModel = require('../models/notifications');
const notification = require('../../config/notification');
module.exports = {
  pushNotification: async (req, res) => {
    console.log("req.body ", req.body);
    var payload = req.body;
    payload.senderId = req.token.id;
    notification.sendNot(payload, (err, results) => {
      console.log("results", results);
      if (err) {
        console.log("erroror innnn");
        return res.json({ status: 0, message: 'Notification Send To Only Active And Logged In Users ', error: err });
      }
      else {
        console.log("*&*&**&*&******pushNotification***** &&& ***** pushNotification   &&&  pushNotification&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        return res.json({ status: 1, results: results })
      }
    })
  },
  signUp: async (req, res) => {
    console.log("======");
    const { email, password, rpassword } = req.body;
    console.log(email, "req.body=====", req.body);
    if (!email) return res.json({ status: 0, message: 'Email is required.' });
    if (!password) return res.json({ status: 0, message: 'Password is required.' });
    if (!rpassword) return res.json({ status: 0, message: 'Re-Password is required.' });
    if (password != rpassword) return res.json({ status: 0, message: 'Password & Re-Password should be same.' });
    if (!usersHelper.emailValidate(email)) return res.json({ status: 0, message: 'Please enter a valid email.' });
    const deviceToken = req.body.deviceToken || '';
    const newPassword = await usersHelper.generateHashPassword(password);
    usersHelper.getEmailInfo(email, null, function (data) {
      if (data.status == 'error') {
        return res.json({ status: 0, message: 'Error in searching email.' });
      }
      if (data && data.data && data.data.length) {
        return res.json({ status: 0, message: 'Email already registered.' });
      }
      let newUser = new userModel({
        email: email.toLowerCase(),
        password: newPassword,
      });
      newUser.save(function (err, results) {
        if (err) {
          console.log(err)
          return res.json({ status: 0, message: 'Error in inserting user.', error: err });
        }
        else {
          var tokenData = {
            id: results._id,
            role: results.role,
            status: results.status
          };
          var token = Jwt.sign(tokenData, privateKey)
          return res.json({ status: 1, message: 'User created successfully.', data: results, token: token });
        }
      });
    });
  },
  login: async (req, res) => {
    const { email, password } = req.body;
    console.log(email, "====email======password===", password);
    if (!email) return res.json({ status: 0, message: 'Email is required.' });
    if (email.length < 5) return res.json({ status: 0, message: 'Email should not be less than 5 characters.' });
    if (!password) return res.json({ status: 0, message: 'Password is required.' });
    const deviceToken = req.body.deviceToken || '';
    if (!usersHelper.emailValidate(email)) return res.json({ status: 0, message: 'Please enter a valid email.' });
    usersHelper.getEmailInfo(email, null, function (data) {
      if (data.status == 'error') {
        return res.json({ status: 0, message: 'Error in searching email.' });
      }
      if (!data || !data.data || !data.data.length) {
        return res.json({ status: 0, message: 'Email do not exists.' });
      }
      var user = data.data && data.data[0] ? data.data[0] : {};
      if (user.status == "active") {
        var comparePassword = usersHelper.comparePassword(password, user.password);
        if (!comparePassword) return res.json({
          status: 0, message: "Oops! The username or password doesn't match."
        });
        delete user.lastActivity;
        delete user.updatedAt;
        delete user.password;
        delete user.lastActivity;
        delete user.updatedAt;
        delete user.otp;
        var tokenData = {
          name: user.name,
          id: user._id,
          role: user.role,
          status: user.status
        };
        var token = Jwt.sign(tokenData, privateKey);
        return res.json({ status: 1, message: 'User logged in successfully.', data: user, token: token });
      }
      else {
        return res.json({ status: 0, message: 'User not authorized.' });
      }
    });
  },
  addGame: async (req, res) => {
    console.log("*********addGame*******addGame******** addGame******addGame******");
    const { startTime, endTime, location, lat, lng, duration, gametype, brandLogo, priceImage } = req.body;
    var files = req.files;
    if (!startTime) return res.json({ status: 0, message: 'Start time is requred' });
    if (!duration) return res.json({ status: 0, message: 'Duration is requred' });
    if (!location) return res.json({ status: 0, message: 'Location is requred' });
    if (!lat && !lng) return res.json({ status: 0, message: 'Lat & Lng are required' });
    if(!gametype) return res.json({ status: 0, message: 'Game Type is required' });
    let splitDate = startTime.split(" ")
    let d = new Date(splitDate[0], splitDate[1], splitDate[2], splitDate[3], splitDate[4], 0, 0);
    if (d < new Date()) return res.json({ status: 0, message: 'Back date can not add game' });
    let newLat, newLong;
    try {
      newLat = parseFloat(lat);
      newLong = parseFloat(lng);
    } catch (e) {
      return res.json({ status: 0, message: 'Cant convert the Lat,Long' });
    }
    var gameModelNew = new gameModel({
      startTime: new Date(startTime),
      duration: duration,
      location: location,
      gametype: gametype,
      geometry: { coordinates: [parseFloat(newLong), parseFloat(newLat)] },
    });
    gameModelNew.save(async function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'Error in save query.', err: err })
      }
      else {
        let imageLogo, priceImg;
        if (files && Object.keys(files).length !== 0) {
          if (files.brandLogo) {
            usersHelper.saveImage(files.brandLogo, data._id, 'brandLogo', function (brandLogoResult) {
              if (brandLogoResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
              let docsNew = new docsModel({
                path: brandLogoResult.data,
                name: brandLogoResult.imageName,
                type: brandLogoResult.imageType,
                size: brandLogoResult.imageSize,
                gameId: data._id
              });
              docsNew.save(function (err, logoData) {
                if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                else {
                  gameModel.findOneAndUpdate({ _id: data._id }, { $set: { brandLogo: logoData._id } }, function (err, updatelogo) {
                    if (err) return res.json({ status: 0, message: 'Error in update logo query' });
                  })
                }
              })
            })
          }
          if (files.priceImage) {
            usersHelper.saveImage(files.priceImage, data._id, 'priceImage', function (priceImageResult) {
              if (priceImageResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
              let docsPriceImage = new docsModel({
                path: priceImageResult.data,
                name: priceImageResult.imageName,
                type: priceImageResult.imageType,
                size: priceImageResult.imageSize,
                gameId: data._id
              });
              docsPriceImage.save(function (err, priceImgData) {
                if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                else {
                  gameModel.findOneAndUpdate({ _id: data._id }, { $set: { priceImg: priceImgData._id } }, function (err, updateImg) {
                    if (err) return res.json({ status: 0, message: 'Error in update Image query' });
                  })
                }
              })
            })
          }
        }
        return res.json({ status: 1, data: data, message: 'Game Saved Successfully.' })
      }
    });
  },
  addTriviaGame: async (req, res)=>{
    console.log("*********addTriviaGame*******addTriviaGame******** addTriviaGame******addTriviaGame******");
    const { startTime, endTime, location, lat, lng, numberofquestion, gametype, duration, brandLogo, priceImage } = req.body;
    var files = req.files;
    if (!startTime) return res.json({ status: 0, message: 'Start time is requred' });
    if (!duration) return res.json({ status: 0, message: 'Duration is requred' });
    if (!location) return res.json({ status: 0, message: 'Location is requred' });
    if (!lat && !lng) return res.json({ status: 0, message: 'Lat & Lng are required' });
    if(!gametype) return res.json({ status: 0, message: 'Game Type is required' });
    let splitDate = startTime.split(" ")
    let d = new Date(splitDate[0], splitDate[1], splitDate[2], splitDate[3], splitDate[4], 0, 0);
    if (d < new Date()) return res.json({ status: 0, message: 'Back date can not add game' });
    let newLat, newLong;
    try {
      newLat = parseFloat(lat);
      newLong = parseFloat(lng);
    } catch (e) {
      return res.json({ status: 0, message: 'Cant convert the Lat,Long' });
    }
    var gameModelNew = new tirivaGameModel({
      startTime: new Date(startTime),
      duration: duration,
      numberofquestion: numberofquestion,
      gametype: gametype,
      location: location,
      geometry: { coordinates: [parseFloat(newLong), parseFloat(newLat)] },
    });
    gameModelNew.save(async function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'Error in save query.', err: err })
      }
      else {
        let imageLogo, priceImg;
        if (files && Object.keys(files).length !== 0) {
          if (files.brandLogo) {
            usersHelper.saveImage(files.brandLogo, data._id, 'brandLogo', function (brandLogoResult) {
              if (brandLogoResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
              let docsNew = new docsModel({
                path: brandLogoResult.data,
                name: brandLogoResult.imageName,
                type: brandLogoResult.imageType,
                size: brandLogoResult.imageSize,
                gameId: data._id
              });
              docsNew.save(function (err, logoData) {
                if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                else {
                  gameModel.findOneAndUpdate({ _id: data._id }, { $set: { brandLogo: logoData._id } }, function (err, updatelogo) {
                    if (err) return res.json({ status: 0, message: 'Error in update logo query' });
                  })
                }
              })
            })
          }
          if (files.priceImage) {
            usersHelper.saveImage(files.priceImage, data._id, 'priceImage', function (priceImageResult) {
              if (priceImageResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
              let docsPriceImage = new docsModel({
                path: priceImageResult.data,
                name: priceImageResult.imageName,
                type: priceImageResult.imageType,
                size: priceImageResult.imageSize,
                gameId: data._id
              });
              docsPriceImage.save(function (err, priceImgData) {
                if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                else {
                  gameModel.findOneAndUpdate({ _id: data._id }, { $set: { priceImg: priceImgData._id } }, function (err, updateImg) {
                    if (err) return res.json({ status: 0, message: 'Error in update Image query' });
                  })
                }
              })
            })
          }
        }
        return res.json({ status: 1, data: data, message: 'Trivia Game Saved Successfully.' })
      }
    });
  },
  editGame: async (req, res) => {
    console.log("*********editGame*******editGame******** editGame******editGame******");
    const { gameId, startTime, location, lat, lng, duration, brandLogo, priceImage } = req.body;
    var files = req.files;
    if (!gameId) return res.json({ status: 0, message: 'gameId is requred' });
    let newLat, newLong;
    try {
      newLat = parseFloat(lat);
      newLong = parseFloat(lng);
    } catch (e) {
      return res.json({ status: 0, message: 'Cant convert the Lat,Long' });
    }
    let gameModelNew = {
      startGame: 0
    }
    if (startTime) { gameModelNew.startTime = req.body.startTime }
    if (duration) { gameModelNew.duration = duration }
    if (req.body.location) { gameModelNew.location = req.body.location }
    if (req.body.lat && req.body.lng) {
      geometry = { coordinates: [parseFloat(newLong), parseFloat(newLat)] }
      gameModelNew.geometry = geometry
    }
    gameModel.findOneAndUpdate({ _id: gameId }, gameModelNew, { upsert: true, new: true },
      (err, upddata) => {
        if (err) { return res.json({ status: 0, message: 'Error in updating game', err: err }); }
        if (upddata) {
          let imageLogo, priceImg;
          if (files && Object.keys(files).length !== 0) {
            if (files.brandLogo) {
              usersHelper.saveImage(files.brandLogo, gameId, 'brandLogo', function (brandLogoResult) {
                if (brandLogoResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
                let docsNew = {
                  path: brandLogoResult.data,
                  name: brandLogoResult.imageName,
                  type: brandLogoResult.imageType,
                  size: brandLogoResult.imageSize,
                  gameId: upddata._id
                };
                docsModel.update({ _id: upddata.brandLogo }, docsNew, (err, logoData) => {
                  if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                  else { return res.json({ status: 0, message: 'Error in brandLogo update.' }) }
                })
              })
            }
            if (files.priceImage) {
              usersHelper.saveImage(files.priceImage, gameId, 'priceImage', function (priceImageResult) {
                if (priceImageResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
                let docsPriceImage = {
                  path: priceImageResult.data,
                  name: priceImageResult.imageName,
                  type: priceImageResult.imageType,
                  size: priceImageResult.imageSize,
                  gameId: upddata._id
                };
                docsModel.update({ _id: upddata.priceImg }, docsPriceImage, (err, priceImgData) => {
                  if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                  if (priceImgData) { console.log("priceImgData", priceImgData); }
                  else { return res.json({ status: 0, message: 'Error in brandLogo update.' }) }
                })
              })
            }
          }
          return res.json({ status: 1, data: upddata, message: 'Game update Successfully.' })
        } else {
          return res.json({ status: 0, message: 'Error in saving updating docs', err: err });
        }
      })
  },
  startGame: async (req, res) => {
    let { gameId } = req.body;
    console.log("req.body============", req.body);
    if (!ObjectId.isValid(gameId)) return res.json({ status: 0, message: 'Game id is not valid' })
    gameModel.findOneAndUpdate({ _id: ObjectId(gameId) }, { $set: { startGame: 2 } }, { new: true }, function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'Error in update query' })
      }
      else {
        return res.json({ status: 1, message: 'Successfully updated', data: data })
      }
    })
  },
  startTriviaGame: async (req, res) => {
    let { gameId } = req.body;
    console.log("req.body============", req.body);
    if (!ObjectId.isValid(gameId)) return res.json({ status: 0, message: 'Game id is not valid' })
    tirivaGameModel.findOneAndUpdate({ _id: ObjectId(gameId) }, { $set: { startGame: 2 } }, { new: true }, function (err, data) {
      if (err) {
        return res.json({ status: 0, message: 'Error in update query' })
      }
      else {
        return res.json({ status: 1, message: 'Successfully updated', data: data })
      }
    })
  },
  gameList: async (req, res) => {
    console.log("*********gameList*******gameList******** gameList******gameList******");
    let docData;
    docsModel.find({
      $or: [{ imageName: 'homeImage' }, { imageName: 'winnerImages' }, { imageName: 'awayImage' }]
    }, function (err, data) {
      if (err) return res.json({ status: 0, message: 'Error in docs data' });
      else {
        docData = data
        gameModel.aggregate([
          {
            // userModel.find({ role: "USERS", "deviceToken": { $exists: true, $ne: "" } }          
            $match: { $or: [{ startGame: 0 }, { startGame: 2 }] }
          },
          {
            $lookup:
            {
              from: "docs",
              localField: 'brandLogo',
              foreignField: '_id',
              as: "brandLogo"
            }
          },
          { $unwind: { "path": "$brandLogo", "preserveNullAndEmptyArrays": true } },
          {
            $project: {
              geometry: 1,
              date: 1,
              startTime: 1,
              location: 1,
              startGame: 1,
              gametype: 1,
              duration: 1,
              createdAt: 1,
              priceImg: 1,
              numberOfHome: { $size: "$home" },
              numberOfAway: { $size: "$away" },
              brandLogo: { $concat: ["http://tugofwar.mobilytedev.com/", "$brandLogo.path"] },
              _id: 1
            }
          },
          {
            $lookup:
            {
              from: "docs",
              localField: 'priceImg',
              foreignField: '_id',
              as: "priceImg"
            }
          },
          { $unwind: { "path": "$priceImg", "preserveNullAndEmptyArrays": true } },
          {
            $project: {
              geometry: 1,
              date: 1,
              startTime: 1,
              location: 1,
              startGame: 1,
              gametype: 1,
              duration: 1,
              createdAt: 1,
              numberOfHome: 1,
              numberOfAway: 1,
              brandLogo: 1,
              priceImg: { $concat: ["http://tugofwar.mobilytedev.com/", "$priceImg.path"] }
            }
          },
          { $sort: { startTime: 1 } }
        ], async function (err, data) {
          if (err) {
            return res.json({ status: 0, message: 'Error in find query' });
          }
          else {
            let time = {}
            if (data.length > 0) {
              for (let each in data) {
                let d = data[each]['startTime']
                let winTeam = {};
                if (docData && docData.length > 0) {
                  if (docData && docData[0].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                  if (docData && docData[0].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                  if (docData && docData[0].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                }
                data[each]['time'] = await moment(d).format("hh:mm A");
                if (data[each]['numberOfHome'] > data[each]['numberOfAway']) data[each]['winTeam'] = 'Home';
                if (data[each]['numberOfHome'] < data[each]['numberOfAway']) data[each]['winTeam'] = 'Away';
                if (data[each]['numberOfHome'] == data[each]['numberOfAway']) {
                  if (data[each]['numberOfHome'] == 0 && data[each]['numberOfAway'] == 0) {
                    data[each]['winTeam'] = 'Pendding'
                  }
                  else {
                    if (data[each]['numberOfHome'] == data[each]['numberOfAway']) data[each]['winTeam'] = 'Tie'
                  }
                }
              }
              return res.json({ status: 1, data: data })
            }
            else {
              return res.json({ status: 0, message: "data not found" })
            }
          }
        })
      }

    });
  },
  triviaGameList: async (req, res) => {
    console.log("*********triviaGameList*******triviaGameList******** triviaGameList******triviaGameList******");
    let docData;
    docsModel.find({
      $or: [{ imageName: 'homeImage' }, { imageName: 'winnerImages' }, { imageName: 'awayImage' }]
    }, function (err, data) {
      if (err) return res.json({ status: 0, message: 'Error in docs data' });
      else {
        docData = data
        tirivaGameModel.aggregate([
          {
            // userModel.find({ role: "USERS", "deviceToken": { $exists: true, $ne: "" } }          
            $match: { $or: [{ startGame: 0 }, { startGame: 2 }] }
          },
          {
            $lookup:
            {
              from: "docs",
              localField: 'brandLogo',
              foreignField: '_id',
              as: "brandLogo"
            }
          },
          { $unwind: { "path": "$brandLogo", "preserveNullAndEmptyArrays": true } },
          {
            $project: {
              geometry: 1,
              date: 1,
              startTime: 1,
              location: 1,
              startGame: 1,
              numberofquestion: 1,
              gametype: 1,
              duration: 1,
              createdAt: 1,
              priceImg: 1,
              numberOfHome: { $size: "$home" },
              numberOfAway: { $size: "$away" },
              brandLogo: { $concat: ["http://tugofwar.mobilytedev.com/", "$brandLogo.path"] },
              _id: 1
            }
          },
          {
            $lookup:
            {
              from: "docs",
              localField: 'priceImg',
              foreignField: '_id',
              as: "priceImg"
            }
          },
          { $unwind: { "path": "$priceImg", "preserveNullAndEmptyArrays": true } },
          {
            $project: {
              geometry: 1,
              date: 1,
              startTime: 1,
              location: 1,
              startGame: 1,
              numberofquestion: 1,
              gametype: 1,
              duration: 1,
              createdAt: 1,
              numberOfHome: 1,
              numberOfAway: 1,
              brandLogo: 1,
              priceImg: { $concat: ["http://tugofwar.mobilytedev.com/", "$priceImg.path"] }
            }
          },
          { $sort: { startTime: 1 } }
        ], async function (err, data) {
          if (err) {
            return res.json({ status: 0, message: 'Error in find query' });
          }
          else {
            let time = {}
            if (data.length > 0) {
              for (let each in data) {
                let d = data[each]['startTime']
                let winTeam = {};
                if (docData && docData.length > 0) {
                  if (docData && docData[0].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'homeImage') data[each].homeImage = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                  if (docData && docData[0].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'winnerImages') data[each].winnerImages = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                  if (docData && docData[0].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[0].path;
                  if (docData && docData.length >= 2 && docData[1].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[1].path;
                  if (docData && docData.length >= 3 && docData[2].imageName == 'awayImage') data[each].awayImage = "http://tugofwar.mobilytedev.com/" + "" + docData[2].path;
                }
                data[each]['time'] = await moment(d).format("hh:mm A");
                if (data[each]['numberOfHome'] > data[each]['numberOfAway']) data[each]['winTeam'] = 'Home';
                if (data[each]['numberOfHome'] < data[each]['numberOfAway']) data[each]['winTeam'] = 'Away';
                if (data[each]['numberOfHome'] == data[each]['numberOfAway']) {
                  if (data[each]['numberOfHome'] == 0 && data[each]['numberOfAway'] == 0) {
                    data[each]['winTeam'] = 'Pendding'
                  }
                  else {
                    if (data[each]['numberOfHome'] == data[each]['numberOfAway']) data[each]['winTeam'] = 'Tie'
                  }
                }
              }
              return res.json({ status: 1, data: data })
            }
            else {
              return res.json({ status: 0, message: "data not found" })
            }
          }
        })
      }

    });
  },
  userInRematch: async (req, res) => {
    rematchModel.aggregate([
      { $match: { rematch: "1" } },
      {
        $project: {
          _id: 1,
          teamName: 1,
          userId: 1,
          teamId: 1,
          createdAt: 1,
          date: { '$dateToString': { format: '%Y-%m-%d', date: "$createdAt" } },
          userName: 1,
          gameTime: 1,
          location: 1,
          duration: 1,
          playedTeam: 1,
          winTeam: 1,
        }
      },
      { $sort: { date: -1 } },
      {
        "$group": {
          "_id": {
            "location": "$location"
          },
          "customerDetails": {
            "$push": {
              "_id": "$_id",
              "date": "$date",
              "teamName": "$teamName",
              "teamId": "$teamId",
              "name": "$userName",
              "userId": "$userId",
              "gameTime": "$gameTime",
              "location": "$location",
              "duration": "$duration",
              "playedTeam": "$playedTeam",
              "winTeam": "$winTeam",
            }
          }
        }
      }
    ], async function (err, data) {
      if (err) {    return res.json({ status: 0, message: 'error in team find query' })  }
      if (data.length) {
        return res.json({ status: 1, rematchUsers: data.length, data: data })
      }
      else { return res.json({ status: 0, message: 'No Data Found' }) }
    })
  },
  addTeam: async (req, res) => {
    const { team } = req.body;
    console.log("team", team);
    if (!team) return res.json(({ status: 0, message: 'Team name is required.' }));
    rematchModel.findOne({ teamName: team, rematch: 0 }, function (err, data) {
      if (err) { return res.json({ status: 0, message: 'error in team find query' }) }
      if (data) { return res.json({ status: 0, message: 'Team Will Same Name Already Exists' }) }
      rematchModel.find({ rematch: 0 }, function (err, rematchData) {
        if (err) {
          return res.json({ status: 0, message: 'error in team find query' })
        }
        if (rematchData && rematchData.length > 2) {
          return res.json({ status: 0, message: 'More than 3 teams not allowed' })
        }
        else {
          let teamNew = new rematchModel({
            teamName: team,
            rematch: 0
          });
          teamNew.save(async function (err) {
            if (err) {
              return res.json({ status: 0, messages: 'Error in team save query.' })
            }
            else {
              return res.json({ status: 1, message: 'successfully save Team' })
            }
          });
        }
      })
    })
  },
  getMainTeams: async (req, res) => {
    rematchModel.find({ rematch: "main" }, function (err, data) {
      if (err) { return res.json({ status: 0, message: 'error in team find query' }) }
      if (data && data.length) { return res.json({ status: 1, data: data }) }
      else {
        return res.json({ status: 0, message: "No Main Teams Found" })
      }
    })
  },
  getTeams: async (req, res) => {
    rematchModel.find({ rematch: 0 }, function (err, data) {
      if (err) { return res.json({ status: 0, message: 'error in team find query' }) }
      if (data && data.length) { return res.json({ status: 1, data: data }) }
      else {
        return res.json({ status: 0, message: "No Teams Found" })
      }
    })
  },
  editTeams: async (req, res) => {
    const { id, teamName } = req.body;
    console.log("team id req.body ", req.body);
    if (!ObjectId.isValid(id)) return res.json({ status: 0, message: 'Id is not valid' })
    if (!teamName) return res.json({ status: 0, message: 'teamName Required' })
    rematchModel.findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: { teamName: teamName }
    }, { upsert: true }, function (err, data) {
      if (err) { return res.json({ status: 0, message: 'Error in   query.' }) }
      else { return res.json({ status: 1, message: 'Successfully edit.' }) }
    })
  },
  deleteAddGame: async (req, res) => {
    let id = req.params.id;
    if (!ObjectId.isValid(id)) return res.json({ status: 0, message: 'Id is not valid' })
    rematchModel.remove({ _id: ObjectId(id) }, function (err, data) {
      if (err) { return res.json({ status: 0, message: 'Error in remove query.' }) }
      else { return res.json({ status: 1, message: 'Successfully removed.' }) }
    })
  },
  rematch: async (req, res) => {
    console.log("+_+_+_ *****rematch******* rematch  *******   rematch  ******* rematch+_+    ");
    const { team, winnerImages, homeImage, awayImage } = req.body;
    let files = req.files;
    if (files && Object.keys(files).length == 0 && !team) return res.json({ status: 0, messages: 'one field is required.' })
    let teamNew;
    if (files && Object.keys(files).length !== 0) {
      async.waterfall([
        function (callback) {
          if (files.winnerImages) {
            usersHelper.saveImage(files.winnerImages, 'winner101', 'winnerImages', function (winnerImagesResult) {
              if (winnerImagesResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
              let docsNew = {
                path: winnerImagesResult.data,
                name: winnerImagesResult.imageName,
                type: winnerImagesResult.imageType,
                size: winnerImagesResult.imageSize,
                imageName: 'winnerImages'
              };
              docsModel.updateOne({ imageName: 'winnerImages' }, { $set: docsNew }, { upsert: true }, function (err, winnerImgData) {
                if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                else {
                  callback(null, winnerImgData);
                }
              })
            })
          }
          else {
            callback(null, []);
          }
        },
        function (recData, callback) {
          if (files.homeImage) {
            usersHelper.saveImage(files.homeImage, 'home101', 'homeImage', function (homeResult) {
              if (homeResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
              let docsNew = {
                path: homeResult.data,
                name: homeResult.imageName,
                type: homeResult.imageType,
                size: homeResult.imageSize,
                imageName: 'homeImage'
              };
              docsModel.updateOne({ imageName: 'homeImage' }, { $set: docsNew }, { upsert: true }, function (err, homeImgData) {
                if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                else {
                  callback(null, []);
                }
              })
            })
          }
          else {
            callback(null, []);
          }
        },
        function (recvData, callback) {
          if (files.awayImage) {
            usersHelper.saveImage(files.awayImage, 'away101', 'awayImage', function (awayResult) {
              if (awayResult.status == 0) return res.json({ status: 0, message: 'Error in upload brand logo.' })
              let docsNew = {
                path: awayResult.data,
                name: awayResult.imageName,
                type: awayResult.imageType,
                size: awayResult.imageSize,
                imageName: 'awayImage'
              };
              docsModel.updateOne({ imageName: 'awayImage' }, { $set: docsNew }, { upsert: true }, function (err, awayImgData) {
                if (err) return res.json({ status: 0, message: 'Error in logo save query.' })
                else {
                  callback(null, []);
                }
              })
            })
          }
          else { callback(null, []); }
        }
      ], function (err, recData) {
        if (err) { return res.json({ status: 0, message: 'Error in uploading images' }); }
        else { return res.json({ status: 1, message: 'successfully upload all images' }) }
      })
    }
  },
  finishedGame: async (req, res) => {
    console.log("*********finishedGame*******finishedGame******** finishedGame******finishedGame******");
    gameModel.aggregate([{ $match: { startGame: 1 } },
    {
      $lookup:
      {
        from: "docs",
        localField: 'brandLogo',
        foreignField: '_id',
        as: "profile"
      }
    },
    { $unwind: { "path": "$profile", "preserveNullAndEmptyArrays": true } },
    {
      $project: {
        geometry: 1,
        date: 1,
        startTime: 1,
        location: 1,
        duration: 1,
        createdAt: 1,
        home: 1,
        winTeam: 1,
        away: 1,
        numberOfHome: { $size: "$home" },
        numberOfAway: { $size: "$away" },
        brandLogo: { $concat: ["~/public/", "$profile.path"] }
      }
    },
    { $sort: { startTime: -1 } }
    ], async function (err, data) {
      if (err) return res.json({ status: 0, message: 'Error in game find query', err: err });
      else {
        if (data && data.length) {
          for (let i = 0; i < data.length; i++) {
            if (data[i].home && data[i].home.length) {
              for (let j = 0; j < data[i].home.length; j++) {
                let ids = data[i].home[j].userId;
                let user = await userModel.find({ _id: ids });
                let arr = _.pluck(user, 'name')
                data[i].home[j].name = arr.join();
              }
            }
            if (data[i].away && data[i].away.length) {
              for (let j = 0; j < data[i].away.length; j++) {
                let ids = data[i].away[j].userId;
                let user = await userModel.find({ _id: ids });
                let arr = _.pluck(user, 'name')
                data[i].away[j].name = arr.join();
              }
            }
          }
                  
        if (data.length > 0) {
          for (let each in data) {
            let hometeamcounttotal = 0, awayteamtotal = 0;
            let winTeam = {};
            console.log("hello")
            for (let value of data[each].away) {
              
              awayteamtotal = awayteamtotal + value.totalCount
            }
            for (let value of data[each].home) {
              
              hometeamcounttotal = hometeamcounttotal + value.totalCount
            }
            //data[each]['numberOfHome'] = hometeamcounttotal;
           // data[each]['numberOfAway'] = awayteamtotal;
           // if (data[each]['numberOfHome'] > data[each]['numberOfAway']) data[each]['winTeam'] = 'Home';
           // if (data[each]['numberOfHome'] < data[each]['numberOfAway']) data[each]['winTeam'] = 'Away';
           console.log("Hometotalcount" + hometeamcounttotal)
           console.log("Teamtotalcount" + awayteamtotal)
           console.log(data[each]['numberOfHome'])
           console.log(data[each]['numberOfAway'])

            if (hometeamcounttotal > awayteamtotal) data[each]['winTeam'] = 'Home';
            if (awayteamtotal > hometeamcounttotal) data[each]['winTeam'] = 'Away';
            if (data[each]['numberOfHome'] > 0 && data[each]['numberOfAway'] == null) data[each]['winTeam'] = 'Home';
            if (data[each]['numberOfHome'] == null && data[each]['numberOfAway'] > 0) data[each]['winTeam'] = 'Away';
            if (data[each]['numberOfHome'] == 0 && data[each]['numberOfAway'] == null) data[each]['winTeam'] = 'Tie';
            if (data[each]['numberOfHome'] == null && data[each]['numberOfAway'] == 0) data[each]['winTeam'] = 'Tie';
            if (data[each]['numberOfHome'] == data[each]['numberOfAway']) {
              if (data[each]['numberOfHome'] == 0 && data[each]['numberOfAway'] == 0) {
                data[each]['winTeam'] = 'Tie'
              }
              else {
                if (data[each]['numberOfHome'] == data[each]['numberOfAway']) data[each]['winTeam'] = 'Tie'
              }
            }
          }
          gameModel.findOneAndUpdate({ _id: data[0]._id },
            { "$set": { winTeam: data[0].winTeam } }, { upsert: true }, (err, upddata) => {
              if (err) { return res.json({ status: 0, message: 'Error in gameModel', err: err }); }
              else {
                console.log(req.body.gameId)
                console.log("Successfully updated game info")
                return res.json({ status: 1, data: data })
              }
            })
        }
         // return res.json({ status: 1, data: data });
        }
        else {
          return res.json({ status: 0, message: "Error in getting data" });
        }

      }
    });
  },
  userDetails: async (req, res) => {
    console.log("*********userDetails*******userDetails******** userDetails******userDetails******");
    let gameId = req.query.id;
    let type = req.query.teamName;
    if (!type) res.return({ status: 0, message: 'game name is required' })
    if (!ObjectId.isValid(gameId)) return res.json({ status: 0, message: 'Game id is not valid' })
    gameModel.find({ _id: ObjectId(gameId) }, function (err, data) {
      if (err) res.return({ status: 1, message: 'Error in find game query.' })
      else {
        userModel.find({ _id: { $in: data[0].type } }, function (err, usersData) {
          if (err) res.return({ status: 0, message: 'Error in users find query' });
          else {
            res.return({ status: 1, data: usersData });
          }
        });
      }
    });
  },
  noGameIdExists: async (req, res) => {
    console.log("*********userDetails*******userDetails******** userDetails******userDetails******");
    docsModel.find({ gameId: { $exists: false } }, function (err, usersData) {
      if (err) return res.json({ status: 0, message: 'Error in users find query' });
      else {
        let arr = [];
        usersData.forEach(item => {
          let obj = item;
          if (item.path != null && item.path != undefined)
            obj.path = `http://tugofwar.mobilytedev.com:8092/${item.path}`;
          arr.push(obj);
        })
        return res.json({ status: 1, data: arr });
      }
    });
  },
  allNotificationsUsers: async (req, res) => {
    userModel.find({ role: "USERS", "deviceToken": { $exists: true, $ne: "" } },
      function (err, data) {
        if (err) { return res.json({ status: 0, message: 'Error in finding data.' }) }
        if (data.length) {
          return res.json({ status: 1, data: data.length, data: data })
        }
        else { return res.json({ status: 0, message: 'NO USER FIND WITH ACTIVE dEVICE TOKEN' }) }
      })
  }

}
