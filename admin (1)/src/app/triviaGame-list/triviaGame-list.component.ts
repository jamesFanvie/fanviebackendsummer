import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { first } from 'rxjs/operators';
import { } from '../websocket.service'
import { CommonService } from '../common.service'
import { WebsocketService } from '../websocket.service'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-triviagame-list',
    templateUrl: './triviaGame-list.component.html',
    styleUrls: ['./triviaGame-list.component.css']
})
export class TriviaGameListComponent implements OnInit {
    games = [];
    socket: any;
    id: string;
    p: number = 1;
    collection: any[];
    closeResult: string;
    allUsers: any;
    notificationArray = [];
    userObjectIdArray = [];
    selectedAll: any;

    constructor(private commonService: CommonService,
        private websocketService: WebsocketService,
        private modalService: NgbModal) { }

    ngOnInit() {
        this.gameList()
    }

    gameList(){
        this.commonService.getTriviaGameList()
        .pipe(first())
        .subscribe(
            data => {
                this.games = data;
                this.socket = this.websocketService.connect();
                //this.router.navigate(['/departments',department.id])
                //this.router.navigate(['/dashboard','users']);
            },
            error => {
                console.log('error', error)
            });
    }

    gameStart(data) {
         this.websocketService.gameStart(data);
        // data.isDisabled=true;

        var startD = new Date(data.startTime);
        startD.setSeconds(0)
        var endD = new Date(data.startTime);
        endD.setSeconds(59);
        console.log(startD.getTime(), "=====startD======endD====", endD.getTime());
        if (data.startGame != 1) {
            if (startD.getTime() <= new Date().getTime() && new Date().getTime() <= endD.getTime()) {
                this.socket.emit('abc', data)
                data.startGame = 1;
                this.commonService.startTriviaGame(data._id)
                    .pipe(first())
                    .subscribe(
                        data => {
                            console.log("data=========", data)
                            if (data.status){
                                alert('Game started !')
                                this.gameList()
                            }
                                else {
                                alert(data.message)
                            }
                        },
                        error => {
                            console.log('error', error)
                        });
            }
            else {
                alert("Before & After time Game not start.")
            }
        }
        else {
            alert("Game already started.")
        }
    }

    open(content) {
        this.notificationArray = [];
        this.commonService.allUsers()
            .pipe(first())
            .subscribe(
                data => {
                    console.log("data=========", data)
                    let tempData = JSON.parse(JSON.stringify(data));
                    let tempArr = [];
                    tempData.data.forEach(element => {
                        console.log(element);
                        let obj = element;
                        obj.selected = false;
                        tempArr.push(obj)
                    });
                    this.allUsers = tempArr;
                },
                error => {
                    console.log('error', error)
                });
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    selectAll() {
        for (var i = 0; i < this.allUsers.length; i++) {
            this.allUsers[i].selected = this.selectedAll;
            if (this.allUsers[i].deviceToken) {
                this.notificationArray.push(this.allUsers[i].deviceToken);
                this.userObjectIdArray.push(this.allUsers[i]._id);
            }
        }
    }

    onSelect(her) {
        if (this.notificationArray.indexOf(her.deviceToken) === -1) {
            if (her.deviceToken) {
                this.notificationArray.push(her.deviceToken);
                this.userObjectIdArray.push(her._id);
            }
        }
        else {
            var index = this.notificationArray.indexOf(5);
            this.notificationArray.splice(index, 1);
            this.userObjectIdArray.splice(index, 1);
        }
    }

    sendNotification() {
        let passingObj = {
            deviceToken: this.notificationArray,
            userId :this.userObjectIdArray
        }
        console.log("this.notification", passingObj);
        this.commonService.sendNotification(passingObj)
            .pipe(first())
            .subscribe(
                data => {
                    if (data.status)
                        alert('Notification sent !')
                    else {
                        alert(data.message)
                    }
                },
                error => {
                    console.log('error', error)
                });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    test() {
        this.commonService.getTriviaGameList()
            .pipe(first())
            .subscribe(
                data => {
                    this.games = data;
                    this.socket = this.websocketService.connect();

                    //this.router.navigate(['/departments',department.id])
                    //this.router.navigate(['/dashboard','users']);
                },
                error => {
                    console.log('error', error)
                });
    };

}
