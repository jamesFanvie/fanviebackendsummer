import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DashboardComponent } from '../dashboard/dashboard.component'
import { CommonService } from '../common.service'
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: []
})
export class UserListComponent implements OnInit {
  // @Input() users: DashboardComponent;
  // @Input() count: number;
  // @Input() UserData:any;
  users = [];
  Counter = 5;
  setStatus = '';
  constructor(private commonService: CommonService) {
  }

  ngOnInit() {
    this.commonService.getUser()
      .pipe(first())
      .subscribe(
        data => {
          this.users = data;
          console.log("data============", this.users)
          //this.router.navigate(['/departments',department.id])
          //this.router.navigate(['/dashboard','users']);
        },
        error => {
          console.log('error', error)
        });
  }

  onDisable(data) {
    data.isDisabled = true;
    if (data.status == "suspended") this.setStatus = "active";
    if (data.status == "active") this.setStatus = "suspended";
    this.commonService.statusChange(data._id, this.setStatus)
      .pipe(first())
      .subscribe(
        dt => {
          for (var k in this.users['data']) {
            if (this.users['data'][k]._id.toString() == data._id.toString()) {
              this.users['data'][k].status = dt.data.status;
              break;
            }
          }

        },
        error => {
          console.log('error', error)
        });
  }

  onDelete(data) {
    this.commonService.deleteUser(data._id)
      .pipe(first())
      .subscribe(
        dt => {
          for (var k in this.users['data']) {
            if (this.users['data'][k]._id.toString() == data._id.toString()) {
              this.users['data'][k].deleted = dt.data.deleted;
              break;
            }
          }

        },
        error => {
          console.log('error', error)
        });
  }

}
