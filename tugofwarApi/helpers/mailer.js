var nodemailer = require('nodemailer');
const env = require('../config/env')();

var transport = nodemailer.createTransport( {
    service: env.SMTP_CRED.email,
    auth: {
        user: env.SMTP_CRED.email,
        pass: env.SMTP_CRED.password
    }
});

module.exports = {
	sendMail: function (data,cb) {
		var message = {
		    from: env.FROM_MAIL,
		    to: data.to,
		    subject: data.subject,
		    text : data.text
		};
		transport.sendMail(message, function(error){
		    if(error){
		    	console.log(error,'error');
		        cb('Error in sending mail');
		    }else{
		      cb(null, {status:1})
		    }
		});
	}
}
