var express = require('express');
var router = express.Router();
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var userController = require('../controllers/usersController');
const authentication = require('../../middlewares/jwtTokenAdmin');
router.post('/teamselect', userController.create);
router.get('/teamselect/:device_id', userController.findOne);
router.post('/joinInTeam', userController.joinInTeam);
router.get('/winningTeam/:gameId', userController.winningTeam);
router.post('/signUp', multipartMiddleware);
router.post('/signUp', userController.signUp);
router.get('/gameDetails/:id', userController.gameDetails)
router.post('/login', userController.login);
router.post('/forgotPassword', userController.forgotPassword);
router.post('/resetPassword', userController.resetPassword)
router.post('/resend', userController.resend);
router.post('/changePassword', authentication, userController.changePassword)
router.post('/totalCount', userController.totalCount)
router.get('/allUsers', userController.allUsers)
router.post('/logoutUser', userController.logoutUser)
router.post('/userRematchTeam', userController.userRematchTeam)
router.get('/userNotificationList', userController.userNotificationList)
router.post('/otpMatch', userController.otpMatch)
router.get('/logoImages', userController.logoImages)
module.exports = router;
