const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const docsSchema = new Schema({
  name: { type: String, default: '' },
  type: { type: String, default: '' },
  path: { type: String, default: '' },
  size: { type: Number },
  imageName: { type: String, default: '' },
  gameId: { type: Schema.ObjectId, ref: 'Games' },
}, { timestamps: true });

docsSchema.set('toObject');
docsSchema.set('toJSON');
module.exports = mongoose.model('Docs', docsSchema);
