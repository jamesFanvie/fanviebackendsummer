import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service'
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  notifi = [];
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.commonService.notificationList()
      .pipe(first())
      .subscribe(
        data => {
          this.notifi = data.data;
          console.log("data============", this.notifi)
          //this.router.navigate(['/departments',department.id])
          //this.router.navigate(['/dashboard','users']);
        },
        error => {
          console.log('error', error)
        });
  }

}
