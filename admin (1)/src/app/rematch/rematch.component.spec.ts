import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RematchComponent } from './rematch.component';

describe('RematchComponent', () => {
  let component: RematchComponent;
  let fixture: ComponentFixture<RematchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RematchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RematchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
